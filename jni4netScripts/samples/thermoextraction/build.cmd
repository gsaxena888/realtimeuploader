rmdir /s /q java
rmdir /s /q csharp
rmdir /s /q target
del listOfJavaFiles.txt 
del listOfCsFiles.txt 

..\..\bin\proxygen.exe thermoextraction.proxygen.xml

mkdir target
copy ..\..\lib\*.* target\

dir /s /B *.java > listOfJavaFiles.txt
mkdir target\classes
javac -cp ../../lib/jni4net.j-0.8.8.0.jar -d target/classes @listOfJavaFiles.txt
jar cvf target/thermoextraction.j4n.jar  -C target\classes .
dir /s /B *.cs > listOfCsFiles.txt

REM Hack...copy over one modified file
REM bbcopy IRawDataPlus.generated.cs .\csharp\thermofisher\commoncore\data\interfaces /Y
REM end of hack

Csc.exe /reference:..\..\lib\jni4net.n-0.8.8.0.dll /reference:.\ThermoFisher.CommonCore.BackgroundSubtraction.dll /reference:.\ThermoFisher.CommonCore.Data.dll /reference:.\ThermoFisher.CommonCore.MassPrecisionEstimator.dll /reference:.\ThermoFisher.CommonCore.RawFileReader.dll /warn:0 /out:target/thermoextraction.j4n.dll /target:library @listOfCsFiles.txt


copy .\target\thermoextraction.j4n.jar ..\..\..\externLib /Y
copy .\target\thermoextraction.j4n.dll ..\..\..\externLib /Y

copy .\target\thermoextraction.j4n.dll ..\..\..\src\main\resources\ /Y
copy .\*.dll ..\..\..\src\main\resources\ /Y
cd ..\..\..\
REM mvn deploy:deploy-file -DgroupId=thermoextraction21 -DartifactId=thermoextraction21 -Dversion=21 -Durl=file:./local-maven-repo/ -DrepositoryId=local-maven-repo -DupdateReleaseInfo=true -Dfile=./externLib/thermoextraction.j4n.jar



