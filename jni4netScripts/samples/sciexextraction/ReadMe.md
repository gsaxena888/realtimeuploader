 private static global::net.sf.jni4net.utils.JniHandle GetCentroidedArray19(global::System.IntPtr @__envp, global::net.sf.jni4net.utils.JniLocalHandle @__obj, int scanNumber)
        {
            // (I)[D;
            // (I)[D;
            global::net.sf.jni4net.jni.JNIEnv @__env = global::net.sf.jni4net.jni.JNIEnv.Wrap(@__envp);
            global::net.sf.jni4net.utils.JniHandle @__return = default(global::net.sf.jni4net.utils.JniHandle);
            try
            {
                global::Clearcore2.Data.DataAccess.SampleData.MSExperiment @__real = global::net.sf.jni4net.utils.Convertor.StrongJp2C<global::Clearcore2.Data.DataAccess.SampleData.MSExperiment>(@__env, @__obj);

                global::Clearcore2.RawXYProcessing.PeakClass[] peakArray = @__real.GetPeakArray(scanNumber);
                int peakArraySize = peakArray.Length;
                double[] mzIntensity = new double[peakArraySize * 2];
                for (int i = 0, j=0; i < peakArraySize; i++, j+=2)
                {
                    mzIntensity[j] = peakArray[i].xValue;
                    mzIntensity[j+1] = peakArray[i].area;
                }

                    //@__return = global::net.sf.jni4net.utils.Convertor.ArrayStrongC2Jp<global::Clearcore2.RawXYProcessing.PeakClass[], global::Clearcore2.RawXYProcessing.PeakClass>(@__env, @__real.GetPeakArray(scanNumber));
                @__return = global::net.sf.jni4net.utils.Convertor.ArrayPrimC2J(@__env, mzIntensity);
            }
            catch (global::System.Exception __ex) { @__env.ThrowExisting(__ex); }
            return @__return;
        }

        =====


        methods.Add(global::net.sf.jni4net.jni.JNINativeMethod.Create(@__type, "GetCentroidedArray", "GetCentroidedArray19", "(I)[D"));


        ====

            @net.sf.jni4net.attributes.ClrMethod("(I)[D")
            public native double[] GetCentroidedArray(int scanNumber);