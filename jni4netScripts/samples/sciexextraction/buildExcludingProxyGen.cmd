rmdir /s /q target
del listOfJavaFiles.txt
del listOfCsFiles.txt 


mkdir target
copy ..\..\lib\*.* target\

dir /s /B *.java > listOfJavaFiles.txt
mkdir target\classes
javac -cp ../../lib/jni4net.j-0.8.8.0.jar -d target/classes @listOfJavaFiles.txt
jar cvf target/sciexextraction.j4n.jar  -C target\classes .
dir /s /B *.cs > listOfCsFiles.txt
Csc.exe /reference:..\..\lib\jni4net.n-0.8.8.0.dll /reference:.\Clearcore2.Data.dll /reference:.\Clearcore2.Data.WiffReader.dll /reference:.\Clearcore2.Data.CommonInterfaces.dll /reference:.\Clearcore2.Data.AnalystDataProvider.dll /reference:.\Clearcore2.Utility.dll /reference:.\Clearcore2.StructuredStorage.dll /reference:.\Clearcore2.RawXYProcessing.dll /reference:.\Clearcore2.ProjectUtilities.dll /reference:.\Clearcore2.Muni.dll /reference:.\Clearcore2.InternalRawXYProcessing.dll /reference:.\Clearcore2.Compression.dll /warn:0 /out:target/sciexextraction.j4n.dll /target:library @listOfCsFiles.txt


copy .\target\sciexextraction.j4n.jar ..\..\..\externLib /Y
copy .\target\sciexextraction.j4n.dll ..\..\..\externLib /Y

copy .\target\sciexextraction.j4n.dll ..\..\..\src\main\resources\ /Y
copy .\*.dll ..\..\..\src\main\resources\ /Y
cd ..\..\..\
mvn deploy:deploy-file -DgroupId=sciexextraction15 -DartifactId=sciexextraction15 -Dversion=15 -Durl=file:./local-maven-repo/ -DrepositoryId=local-maven-repo -DupdateReleaseInfo=true -Dfile=./externLib/sciexextraction.j4n.jar
cd .\jni4netScripts\samples\sciexextraction


