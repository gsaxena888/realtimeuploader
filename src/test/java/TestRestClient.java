import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import systems.deepsearch.common.pojo.general.*;
import systems.deepsearch.common.pojo.ms.MsFile;
import systems.deepsearch.common.util.Constants;
import systems.deepsearch.common.util.RestApiPaths;
import systems.deepsearch.common.util.Util;


import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Slf4j
public class TestRestClient {
    private static final String APP_URL = "http://127.0.0.1:8080"; //THIS CAN BE CHANGED AS NEEDED
    private static final String BASE_URI = APP_URL + RestApiPaths.BASE_API_PATH;
    private final Client client = ClientBuilder.newClient();
    private final WebTarget apiUrl = client.target(BASE_URI);


    @Test
    public void testImportMsFileAssumingQueued() {

        //create dummy object. TECHNICALLY, this is not 100% correct since this recreation of the dummy ojbects will crate NEW objectIds, but it happens to be good enough for this test, since, ultimately, the "importMsFile" restAPI never compares anything using objet ids, but instead does queries using the "xxHash64" object or the fileName....
        Util.CreateBasicDummyObjects createBasicDummyObjects = new Util.CreateBasicDummyObjects().invoke();
        User deepSearchAdminUser = createBasicDummyObjects.getDeepSearchAdminUser();
        User user1 = createBasicDummyObjects.getUser1();
        User user2 = createBasicDummyObjects.getUser2();
        Company companyOfAdminUser = createBasicDummyObjects.getCompanyOfAdminUser();
        Company companyOfCustomers = createBasicDummyObjects.getCompanyOfCustomers();
        Lab lab1 = createBasicDummyObjects.getLab1();
        RtiMachineInfoDto rtiMachineInfo = createBasicDummyObjects.getRtiMachineInfo().createDto();
        final MsFile msFileFromRti = Util.createDummyMsFileThatNeverMatched("someHashOfSomeSort", Util.MS_FILE2, 20, rtiMachineInfo);

        final List<String> signedUrls = apiUrl
                .path(RestApiPaths.IMPORT_MS_FILE)
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(msFileFromRti, MediaType.APPLICATION_JSON))
                .readEntity(new GenericType<List<String>>() {});
        log.info("signedUrls is {}", signedUrls);
    }

//   // @Test
//    public void testImportMsFileNeverMatched() {
//
//        //create dummy object. TECHNICALLY, this is not 100% correct since this recreation of the dummy ojbects will crate NEW objectIds, but it happens to be good enough for this test, since, ultimately, the "importMsFile" restAPI never compares anything using objet ids, but instead does queries using the "xxHash64" object or the fileName....
//        Util.CreateBasicDummyObjects createBasicDummyObjects = new Util.CreateBasicDummyObjects().invoke();
//        User deepSearchAdminUser = createBasicDummyObjects.getDeepSearchAdminUser();
//        User user1 = createBasicDummyObjects.getUser1();
//        User user2 = createBasicDummyObjects.getUser2();
//        Company companyOfAdminUser = createBasicDummyObjects.getCompanyOfAdminUser();
//        Company companyOfCustomers = createBasicDummyObjects.getCompanyOfCustomers();
//        Lab lab1 = createBasicDummyObjects.getLab1();
//        RtiMachineInfo rtiMachineInfo = createBasicDummyObjects.getRtiMachineInfo();
//        final MsFile msFileFromRti = Util.createDummyMsFileThatNeverMatched(Util.XX_HASH_64_FOR_CUSTOM_FILE, Util.CUSTOM_FILE, 20, rtiMachineInfo);
//
//        final List<String> signedUrls = client.target(BASE_URI)
//                .path("importMsFile")
//                .request(MediaType.APPLICATION_JSON)
//                .post(Entity.entity(msFileFromRti, MediaType.APPLICATION_JSON))
//                .readEntity(new GenericType<List<String>>() {});
//        log.info("signedUrls is {}", signedUrls);
//    }

}
