package com.deepdia.realtimeuploader;




import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.esotericsoftware.kryo.util.DefaultInstantiatorStrategy;
import com.google.common.base.Preconditions;
import com.google.common.math.DoubleMath;

import it.unimi.dsi.fastutil.floats.FloatArrayList;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.commons.math3.util.FastMath;
import org.objenesis.strategy.StdInstantiatorStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.concurrent.*;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterOutputStream;



//import org.nustaq.serialization.FSTConfiguration;
//import systems.deepsearch.common.pojo.backendrepo.ConfigSettings;
//import systems.deepsearch.common.pojo.backendrepo.ConfigSettings;
//import systems.deepsearch.common.pojo.backendrepo.ConfigSettings;

//import static com.deepdia.deepsearch.mainprograms.DoDeepSearchStage1.tinyThreadPoolForNetwork;

//@Value.Immutable
//@Value.Style(init = "set*", stagedBuilder = true, strictBuilder = true)
////@Entity(noClassnameStored = true)
public abstract  class MyUtils implements Serializable {

    public static final ThreadLocal<Kryo> kryoTl = initialize();

    private static ThreadLocal<Kryo> initialize() {
        return ThreadLocal.withInitial(MyUtils::createKryo);
    }

    private static Kryo createKryo() {
        final Kryo kryo = new Kryo();//createKryo();
        kryo.setInstantiatorStrategy(new DefaultInstantiatorStrategy(new StdInstantiatorStrategy()));
        kryo.setReferences(true);
        kryo.setCopyReferences(true);
        kryo.setRegistrationRequired(false);

        //kryo.register(MsFile.class);
        //kryo.register(MsFile.class);


        return kryo;
    }


    public static final DecimalFormat frmtSciWith2Dec = new DecimalFormat("0.00E0");
    public static final DecimalFormat frmtSciWith3Dec = new DecimalFormat("0.000E0");
    public static final DecimalFormat frmtSciWith8Dec = new DecimalFormat("0.00000000E0");
    public static final DecimalFormat frmtWith0Dec = new DecimalFormat("####0");
    public static final DecimalFormat frmtWith3Dec = new DecimalFormat("####0.000");
    public static final DecimalFormat frmtPercWith1Dec = new DecimalFormat("####0.0%");


    public static final String FOLD_SEP = "/";
    private static final long serialVersionUID = 1L;
    private static final Logger LOG = LoggerFactory.getLogger(MyUtils.class);
    public static final  OperatingSystemMXBean osBean = ManagementFactory.getPlatformMXBean(OperatingSystemMXBean.class);
    public final static boolean SAFE_MODE = false;
    //public static final float ORBI_RESOLUTION_AT_200Th = 15000f; //30000f;



    public static int getNumThreads() {
        return 1 + ForkJoinPool.getCommonPoolParallelism(); // the java threads stream on common fork pool PLUS the main thread, hence the addition of 1
        //return ForkJoinPool.getCommonPoolParallelism(); // the java threads stream on common fork without the plus 1
    }

 
    public static boolean isAssertOn() {
        boolean assertOn = false;
// *assigns* true if assertions are on.
        assert assertOn = true;
        return assertOn;
    }

    public static double roundToN(final double doubleVal, final int numDec) {
        final double scaler  = numDec == 1 ? 10 : FastMath.pow(10, numDec);
        return DoubleMath.roundToInt(doubleVal * scaler, RoundingMode.HALF_UP) / scaler;
    }

    public static boolean isEven(int i) {
        return (i & 1) == 0;
    }

    public static double powIntReal(float error, int i) {
        error = FastMath.abs(error);
        for (int j = 1; j < i; j++) {
            error *= error;
        }
        return error;
    }

    public static double powInt(float error, float i) {
        error = FastMath.abs(error);
        return FastMath.pow(error, i);
    }


    //the index of the search key, if it is contained in the list; otherwise, (-(insertion point) - 1). The insertion point is defined as the point at which the key would be inserted into the list: the index of the first element greater than the key, or list.size() if all elements in the list are less than the specified key. Note that this guarantees that the return value will be >= 0 if and only if the key is found.
    public static int indexedBinarySearch(FloatArrayList list, float key) {
        int low = 0;
        int high = list.size()-1;

        while (low <= high) {
            int mid = (low + high) >>> 1;
            float midVal = list.getFloat(mid);
            int cmp = Float.compare(midVal, key);

            if (cmp < 0)
                low = mid + 1;
            else if (cmp > 0)
                high = mid - 1;
            else
                return mid; // key found
        }
        return -(low + 1);  // key not found
    }

    public static float sigmoid(float x, int a, int b) {
        //1/(1+e^(-5x+2))
        return 1f/(1+ MyUtils.approxExp(a*x+b));
    }

    public static double approxLogFor0to1(double x) {
        //assert x >= -0.8f;
        //assert x <= 1.8f;
        x = Math.min(0.999,Math.max(x, 0.001));
        //if (x <= 0.001) x = 0.001;
        //if (x >= 0.999) x = 0.999;
        return DoubleMath.log2(x); //TODO replace with fast implementation using lookup tables
    }


    // ********************************
    // vars
    // *************************************

   // public abstract List<String> getInputMzXmlFilesList();

    //public abstract String getOutputFolder();

    //public abstract  float getBaselineLevel();

    //public abstract  int getNumPointsPerFwhm();

    //public abstract  int getNumIntervalsForNewRegion();

    //public abstract  int getProbeRtWidth();

    //public abstract  int getProbeMzWidth();

    /*
    public abstract  int getMinFeatureRtWidthInCycles();

    public abstract  int getMaxFeatureRtWidthInCycles();

    public abstract  int getMinFeatureRtWidthInCyclesForClustering();

    public abstract  int getMaxFeatureRtWidthInCyclesForClustering();

   //     public abstract float getSmallFeatureCorrelationPenalty();

    public abstract  float getMaxDistanceBetweenFeatures();

    public abstract  float getMaxAggregateDistanceInCluster();

    public abstract float getMaxRtDistanceForClustering();

    public abstract  int getMinFeaturesInCluster();

    //public abstract  boolean isSaveRawSpectra();

    //public abstract  boolean isSaveDeconvolutedMzSpectra();

    //public abstract  boolean isSaveFeatures();

    //public abstract  boolean isSaveRepresentativeFeatures();

    //public abstract  int getMinDIAwindowToProcess();

*/
    // ************************
    // NON-STATIC UTILITY METHODS
    // ************************

    //public abstract  int getMaxDIAwindowToProcess();

    //public abstract  float getMinRtInMinutesToProcess();

    //public abstract  float getMaxRtInMinutesToProcess();

    //public abstract  float getMinMzToProcess();

    //public abstract  float getMaxMzToProcess();

    public  boolean isSafeMode() { return SAFE_MODE;}; //TODO eventually, getDGivenIdx rid of this method and replace all logic to just referrence directlty the static constant SAFE_MODE....this should be cleaner, maybe even faster...

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }


    // ***************************
    // NON-static UTILITY METHODS
    // *****************************

    public static float interpolate(double leftMz, float leftIntensity, double rightMz, float rightIntensity,
                             double middleMz) {
        if (SAFE_MODE) {
            Preconditions.checkState(rightMz > leftMz,
                    "Your  rightMz of %s was lower or equal to leftMz of %s", leftMz, rightMz);
        }

        if (middleMz > rightMz) middleMz = rightMz;
        else if (middleMz < leftMz) middleMz = leftMz;

        double deltaMz = rightMz - leftMz;
        float deltaIntensity = rightIntensity - leftIntensity;
        double fractionalPartOfMz = (middleMz - leftMz) / deltaMz;

        return (float) (leftIntensity + (fractionalPartOfMz * deltaIntensity));
    }

    public float roundFloatIntensity(float intensity) {
        return (float) Math.round(intensity * 10f) / 10f;
    }

    public float roundFloatRt(float rt) {
        return (float) Math.round(rt * 100f) / 100f;
    }

    public double roundDoubleMz(double mz) {
        return (double) Math.round(mz * 1000f) / 1000f;
    }

    public double ppmToMass(double ppm, double mass) {
        return (ppm / 1e6) * mass;
    }



    public static int[] convertFloatsToInts(float[] input)
    {
        if (input == null)
        {
            return null; // Or throw an exception - your choice
        }
        int[] output = new int[input.length];
        for (int i = 0; i < input.length; i++)
        {
            output[i] = DoubleMath.roundToInt(input[i], RoundingMode.HALF_UP);
        }
        return output;
    }

    public static double[] convertFloatsToDoubles(float[] input)
    {
        if (input == null)
        {
            return null; // Or throw an exception - your choice
        }
        double[] output = new double[input.length];
        for (int i = 0; i < input.length; i++)
        {
            output[i] = input[i];
        }
        return output;
    }

    public static double[] convertIntsToDoubles(int[] input) {
        if (input == null)
        {
            return null; // Or throw an exception - your choice
        }
        double[] output = new double[input.length];
        for (int i = 0; i < input.length; i++)
        {
            output[i] = input[i];
        }
        return output;
    }

    public static float[] convertIntsToFloats(int[] input)
    {
        if (input == null)
        {
            return null; // Or throw an exception - your choice
        }
        float[] output = new float[input.length];
        for (int i = 0; i < input.length; i++)
        {
            output[i] = input[i];
        }
        return output;
    }

    public static float[] convertDoublesToFloats(double[] input)
    {
        if (input == null)
        {
            return null; // Or throw an exception - your choice
        }
        float[] output = new float[input.length];
        for (int i = 0; i < input.length; i++)
        {
            output[i] = (float) input[i];
        }
        return output;
    }



    /* fast floating point exp function
 * must initialize table with buildexptable before using

Based on
 A Fast, Compact Approximation of the Exponential Function
 Nicol N. Schraudolph 1999

Adapted to single precision to improve speed and added adjustment table to improve accuracy.
Alrecenk 2014

 * i = ay + b
 * a = 2^(mantissa bits) / ln(2)   ~ 12102203
 * b = (exponent bias) * 2^ ( mantissa bits) ~ 1065353216
 */
//    private static AtomicInteger countGood = new AtomicInteger(0);
//    private static AtomicInteger countBad = new AtomicInteger(0);
    public static float approxExp(float x){
    //    return (float) FastMath.exp(x);
        if (x < minExp) {
            return minExpValue;
        }
        else if (x > maxExp) {
            return maxExpValue;
        }
        final int temp = (int)(12102203 * x + 1065353216) ;
        final float approxExp = Float.intBitsToFloat(temp) * expadjust[(temp >> 15) & 0xff];
        return approxExp;
    }

    static float expadjust[] ;
    static double minExp;
    static double maxExp;
    static float minExpValue;
    static float maxExpValue;

    //build correction table to improve result in region of interest
//if region of interest is large enough then improves result everywhere
    public static void buildexptable(double min, double max, double step){
        minExp = min;
        maxExp = max;
        minExpValue = (float) FastMath.exp(min);
        maxExpValue = (float) FastMath.exp(max);
        expadjust = new float[256];
        int amount[] = new int[256] ;
        //calculate what adjustments should have been for values in region
        for(double x=min; x < max;x+=step){
            double exp = Math.exp(x);
            int temp = (int)(12102203 * x + 1065353216) ;
            int index = (temp>>15)&0xff ;
            double fexp = Float.intBitsToFloat(temp);
            expadjust[index]+= exp/fexp ;
            amount[index]++;
        }
        //average them out to getDGivenIdx adjustment table
        for(int k=0;k<amount.length;k++){
            expadjust[k]/=amount[k];
        }
    }

    //TODO make a "resolutionEstimator" class, simliar to emgCoefficients
//    public static double estimateFwhm(float mz) {
//        //FOR now, we've hardcoded this for sciex intstruments
////        final double resolution = 15000;
////        final double fwhm = mz / resolution;
////        return fwhm;
//
//        //TODO remove hardcoding for thermo
//      //  final float ppmTol = 50f;
//       // return (ppmTol * mz) / (1e6 * DoDeepSearchStage1.MAX_FRACTION_OF_FWHM_FOR_FEATURE_LINKING);
//        return (float) (mz/(ORBI_RESOLUTION_AT_200Th /FastMath.sqrt(mz/200f)));
//    }

//    public static <T> List<CompletableFuture<Boolean>> safeDbSaveList(Datastore db, List<T> ps) {
//        return safeDbSaveList(db, ps, null);
//    }
//
//    public static <T> List<CompletableFuture<Boolean>> safeDbSaveList(Datastore db, List<T> ps, ProgressCounter pc) {
//        return ps.stream().map(aPs -> safeDbSave(db, aPs, pc)).collect(Collectors.toList());
//    }
//
//    public static CompletableFuture<Boolean> safeDbSave(Datastore db, Object ps) {
//        return safeDbSave(db, ps, null);
//    }
//
//    public static CompletableFuture<Boolean> safeDbSave(Datastore db, Object ps, ProgressCounter pc) {
//        assert !(ps instanceof List) ;
//        return CompletableFuture.supplyAsync(() -> {
//            final Boolean isSuccess = tryNTimes(MAX_TRIES_FOR_MORPHIA_SAVE, 1.0f, () -> {
//                doSave(db, ps);
//                if (pc != null) {
//                    pc.step();
//                }
//                return true;
//            });
//            if (isSuccess == null || !isSuccess) {
//               // LOG.error("The first few characters of the failed-to-save object's tostring is:\n{}", ps.toString().substring(0, 1_000));
//                LOG.error("The first few characters of the failed-to-save object's tostring is NOT GOING TO BE DISPLAYED");
//                return false;
//            }
//            return true;
//        }, tinyThreadPoolForNetwork);
//    }
//
//    public static <T> T tryNTimes(final int maxTries, final float secDelay, Callable<T> op) {
//        int tries = 1;
//        Exception lastException = null;
//        do {
//            try {
//                return op.call();
//            } catch (Exception ex) {
//                LOG.warn("Exceptionw while trying to do op. Ex is {} for try # {}", ex, tries);
//            //    ex.printStackTrace();
//                DsOptionsAndUtilities.sleepInSec(secDelay * tries);
//                tries++;
//                lastException = ex;
//            }
//        } while (tries <= maxTries);
//        LOG.error("Tried " + maxTries + " times but still failed to run op. we'll return null");
//        lastException.printStackTrace();
//      //  //System.exit(-1);
//        return null;
//    }
//
//    private static void doSave(Datastore db, Object ps) {
//        final Key<Object> key = db.save(ps);
//        if (key == null || key.getId() == null) {
//            throw new RuntimeException("Key or Key Id is null for ps of " + ps.toString().substring(0, 250));
//        }
//        return;
//    }

    public static double approxPowReal(final double a, final double b) {
        // exponentiation by squaring
        double r = 1.0;
        int exp = (int) b;
        double base = a;
        while (exp != 0) {
            if ((exp & 1) != 0) {
                r *= base;
            }
            base *= base;
            exp >>= 1;
        }

        // use the IEEE 754 trick for the fraction of the exponent
        final double b_faction = b - (int)b;
        final long tmp = Double.doubleToLongBits(a);
        final long tmp2 = (long) (b_faction * (tmp - 4606921280493453312L)) + 4606921280493453312L;
        return r * Double.longBitsToDouble(tmp2);
    }

    public static float approxPow(final float x, final float exp) {
        if (exp == 1f) return x;
        if (exp == 2f) return x * x;
        if (exp == 3f) return x * x * x;
        if (exp == 4f) return x * x * x * x;
      //  LOG.debug("Can't do fast approx pow since exp is {}; just doing normal FastMath.pow()", exp);
        return (float) FastMath.pow(x, exp); //TODO replace with approximate version
    }

//    public static void tryLockFor60Seconds(Lock lock) {
//        try {
//            lock.tryLock(60, TimeUnit.SECONDS);
//        } catch (InterruptedException e) {
//            throw new RuntimeException("Lock timeout exceeded 60 seconds for lock " + lock);
//        }
//    }



//    public static double estimateFwhm(InstrumentType instrumentType, double mz, float minIForTol) {
//
//    }
//    public double getResolutionForMz(double mz, double resolution, double resolutionAt, ResolutionModel resolutionModel) {
//        switch (resolutionModel) {
//            case CONSTANT:
//                return resolution;
//
//            case LINEAR:
//                return resolution * (resolutionAt / mz);
//
//            case SQRT:
//                return resolution * (Math.sqrt(resolutionAt / mz));
//
//            default:
//                return resolution;
//        }
//    }

    public static int getLowerOrEqualPoint(final int idx) {
        return (idx < 0) ? -(idx + 1) - 1: idx;
    }

    public static String prettyOutputOf2DDoubleArray(double[][] costMatrixAs2DArray) {
        final StringBuilder sb = new StringBuilder();
        sb.append("\nFrom 2d double array: NumNodes: ");
        sb.append(costMatrixAs2DArray.length);
        sb.append("\nw,j,weight\n");
        for (int w = 0, size = costMatrixAs2DArray.length; w < size; w++) {
            for (int j = 0; j < size; j++) {
                sb.append(w);
                sb.append(",");
                sb.append(j);
                sb.append(",");
                sb.append( String.format("%.6f", costMatrixAs2DArray[w][j]));
                sb.append("\n");
            }
        }

        return sb.toString();
    }

    public static String prettyOutputOf1DDoubleArray(double[] costMatrixAs2DArray) {
        final StringBuilder sb = new StringBuilder();
        for (int w = 0, size = costMatrixAs2DArray.length; w < size; w++) {
                sb.append( String.format("%.6f", costMatrixAs2DArray[w]));
                sb.append(",");
        }

        return sb.toString();
    }

    public static String prettyOutputOf1DFloatArray(float[] costMatrixAs2DArray) {
        final StringBuilder sb = new StringBuilder();
        for (int w = 0, size = costMatrixAs2DArray.length; w < size; w++) {
            sb.append( String.format("%.6f", costMatrixAs2DArray[w]));
            sb.append(",");
        }

        return sb.toString();
    }

    public static void sleepInSec(final float secondsToSleep) {
        LOG.warn("About to sleep for {} seconds", secondsToSleep);
        try {
            Thread.sleep(DoubleMath.roundToInt(secondsToSleep * 1000f, RoundingMode.HALF_UP));
        } catch (InterruptedException e) {
            LOG.warn("Sleeping interupted");
            e.printStackTrace();
        }
    }

    public static byte[] compress(byte[] in) {
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            DeflaterOutputStream defl = new DeflaterOutputStream(out);
            defl.write(in);
            defl.flush();
            defl.close();

            return out.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
            //System.exit(150);
            return null;
        }
    }

    public static byte[] decompress(byte[] in) {
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            InflaterOutputStream infl = new InflaterOutputStream(out);
            infl.write(in);
            infl.flush();
            infl.close();

            return out.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
            //System.exit(150);
            return null;
        }
    }

    public static float detLast2Digits(double val) {
        val /= 100f;
        return (float) (val - DoubleMath.roundToInt(val, RoundingMode.FLOOR));
    }

//    public static String retBucketName() {
//
//        final ConfigSettings config = ConfigSettings.getInstance();
//        final StringBuilder sb = new StringBuilder();
//        sb.append(config.getAppName());
//        sb.append("--");
//        sb.append(config.getEnv().toString());
//        sb.append("--");
//        sb.append("us-east4"); //TODO fix to not hardcode....
//        assert sb.toString().equals("deepsearch--dev--us-east4");
//        return sb.toString();
//    }



    public static <K> K deserialize(Class<K> type, byte[] bytes) {
        if (bytes == null || bytes.length==0) return null;
        final Input input = new Input(bytes);
        final K ret = kryoTl.get().readObject(input, type);
        input.close();
        return ret;
    }

    public static <K> K deserializeInputStream(Class<K> type, InputStream inputStream) {
        final Input input = new Input(inputStream);
        final K ret = kryoTl.get().readObject(input, type);
        input.close();
        return ret;
    }

    public static byte[] serialize(Object str) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        serializeStream(str, baos);
        return baos.toByteArray();
    }


    public static void serializeStream(Object str, OutputStream baos) {
        try (Output output = new Output(baos)) {
            kryoTl.get().writeObject(output, str);
        }
    }


}