package com.deepdia.realtimeuploader.extractmsdata.sciexextractor;

import clearcore2.data.dataaccess.sampledata.*;
import clearcore2.licensing.LicenseKeys;
import com.deepdia.realtimeuploader.extractmsdata.*;
import com.google.common.util.concurrent.AtomicDouble;
import systems.deepsearch.common.pojo.ms.embedded.ImmutableDIAWindowHeader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import systems.deepsearch.common.pojo.ms.ImmutableDeconvolutedMzSpectrum;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.DoubleAccumulator;
import java.util.concurrent.atomic.LongAccumulator;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class DoSciexExtraction implements InputFileExtractor {
    private static final Logger LOG = LoggerFactory.getLogger(DoSciexExtraction.class);
    private static String[] ABI_BETA_LICENSE_KEY = new String[]{"<?xml version=\"1.0\" encoding=\"utf-8\"?><license_key><company_name>Proteo Wizard|Redistributable Beta Agreemeent 2012-03-20</company_name><product_name>ProcessingFramework</product_name><features>WiffReaderSDK</features><key_data>Z9360tXYtYTi+Ru6cbynF52hEBoKTFNHT0vYGPCI5ZmmAXRZ+W26Ag==</key_data></license_key>"};
    private final int experimentCount;
    private final ThreadLocal<WiffReader> localWiffReader;
    private final ImmutableSciexHeaderInfo sciexHeaderInfo;
    private final String inputFile;
    private int totalMs2Scans;

    public DoSciexExtraction(File inputFileAsFile) {
        String inputFile = inputFileAsFile.getAbsolutePath();
        //load the Sciex dll
        final Jni4NetSingleton jni4net = Jni4NetSingleton.getInstance();
        jni4net.loadDll("sciexextraction.j4n.dll");
        LicenseKeys.setKeys(ABI_BETA_LICENSE_KEY);

        localWiffReader = ThreadLocal.withInitial(() -> new WiffReader(inputFile));
        this.inputFile = inputFile;
        //  LOG.info("det.getAcquisitionMethodName() {}, det.getAcquisitionDateTime() {},  det.getDADDetectorName() {}, det.getDilutionFactor() {}, det.getExtraProperties() {}, det.getInjectionVolume() {}, det.getInstrumentName() {}, det.getInstrumentSerialNumber() {}, det.getNumCustomFields() {}, det.getNumPeakConcentrations() {}, det.getBatchName() {}, det.getDilutionFactor() {}, det.getAcquisitionMethodName() {}, det.getPlate() {}, det.getRack() {}, det.getSampleComment() {}, det.getSampleID() {}, det.getSampleName() {}, det.getInstrumentSerialNumber() {}, det.getInjectionVolume() {}, det.getSampleType() {}, det.getUserName() {}, det.getVial() {}", det.getAcquisitionMethodName(), det.getAcquisitionDateTime(), det.getDADDetectorName(), det.getDilutionFactor(), det.getExtraProperties(), det.getInjectionVolume(), det.getInstrumentName(), det.getInstrumentSerialNumber(), det.getNumCustomFields(), det.getNumPeakConcentrations(), det.getBatchName(), det.getDilutionFactor(), det.getAcquisitionMethodName(), det.getPlate(), det.getRack(), det.getSampleComment(), det.getSampleID(), det.getSampleName(), det.getInstrumentSerialNumber(), det.getInjectionVolume(), det.getSampleType(), det.getUserName(), det.getVial());
        this.experimentCount = localWiffReader.get().getMassSepctrometerSample().getExperimentCount();
        LOG.info("There are {} MS1 and MS2 experiments/windows", experimentCount);

        final IntStream experimentCountStream = IntStream.range(1, experimentCount); //TODO for now, we're only pulling MS2 info; but we should change that....note: if we change that logic here, we should also adjust it in the mzXML extractor logic as well


        final HashMap<Integer, ImmutableDIAWindowHeader> diaWindowHeaderMap = (HashMap<Integer, ImmutableDIAWindowHeader>) experimentCountStream
                .asLongStream()
                .sequential() // no real value in making parallel (might even be slower); but if you do make paralle, totalMs2Scans needs to become AtomicInt
                .mapToObj(exp -> {
                    final int expNum = (int) (long) exp;
                    final MSExperiment experiment = localWiffReader.get().getMassSepctrometerSample().GetMSExperiment(expNum);
                    final MSExperimentInfo details = experiment.getDetails();
                    totalMs2Scans += details.getNumberOfScans();

                    final ExperimentType expType = details.getExperimentType();
                    // final boolean isProductScan = expType.ToString().equals("Product");
                    MassRange[] massRangeInfo = details.getMassRangeInfo();
                    final FragmentBasedScanMassRange fragmentBasedScanMassRange = (FragmentBasedScanMassRange) massRangeInfo[0];
                    final double centerMz = fragmentBasedScanMassRange.getFixedMasses()[0];
                    final double width = fragmentBasedScanMassRange.getIsolationWindow();
                    final double startMz = centerMz - (width / 2d);
                    final double endingMz = centerMz + (width / 2d);

                    final float rt = (float) experiment.GetRTFromExperimentScanIndex(0);
//                    return ImmutableDIAWindowHeader
//                            .builder()
//                            .setDIAWindowNumber(expNum - 1)
//                            .setDIAWindowMzCenter(centerMz)
//                            .setDIAWindowMzWidth(width)
//                            .setFirstRtMin(rt)
//                            .build();
                    return new ImmutableDIAWindowHeader(expNum - 1, centerMz, width, rt);
                })
                .collect(Collectors.toMap(diaWindowHeader -> diaWindowHeader.getDiaWindowNumber(), diaWindowHeader -> diaWindowHeader));

        sciexHeaderInfo = ImmutableSciexHeaderInfo
                .builder()
                .setFileName(inputFile)
                .setMsManufacturer("Sciex") //todo verify the exact name used in the mzxml files and use that one only (inlcuidng case sensitivitiy)
                .setMsModel("TBD") //todo getDGivenIdx this value from actual wiff file and also make sure it matches mzXML file settings.
                .setAllDIAWindowHeadersByDiaWindowNum(diaWindowHeaderMap)
                .setNumDIAWindows(experimentCount - 1)
                .build();
    }


    @Override
    public Stream<ImmutableRawDiaSpectrum> getRawSpectrumStream(int minDiaWindowToProcess, int maxDiaWindowToProcess, float minRtToProcess, float maxRtToProcess, double minMzToProcess, double maxMzToProcess, final float baseline) {
        throw new RuntimeException("No longer supported"); // we only now support getting centroided data out; later, if useful, we can revist this...
    }

    //OLD code below for when we were doing our own centroiding....
//    @Override
//    public Stream<ImmutableRawDiaSpectrum> getRawSpectrumStream(int minDiaWindowToProcess, int maxDiaWindowToProcess, float minRtToProcess, float maxRtToProcess, double minMzToProcess, double maxMzToProcess, final float baseline) {
//        minDiaWindowToProcess += 1;
//        if (minDiaWindowToProcess <= 0) minDiaWindowToProcess = 1;
//        else if (minDiaWindowToProcess >= experimentCount) return null; //nothing to process
//
//        if (maxDiaWindowToProcess != Integer.MAX_VALUE) maxDiaWindowToProcess += 1; //since exp = 0 ==> ms1
//        if (maxDiaWindowToProcess >= (experimentCount)) maxDiaWindowToProcess = experimentCount - 1;
//        if (maxDiaWindowToProcess <= 0) return null; //makes no sense.
//        LOG.info("MinDiaWinow is {} and MaxDiaWindow is {}", minDiaWindowToProcess, maxDiaWindowToProcess);
//        final IntStream experimentCountStream = IntStream.range(minDiaWindowToProcess, maxDiaWindowToProcess + 1); //TODO for now, we're only pulling MS2 info; but we should change that....note: if we change that logic here, we should also adjust it in the mzXML extractor logic as well
//
//        return experimentCountStream
//                .asLongStream()
////                .peek(exp -> LOG.info("ExpCount is {}", exp))
//                .parallel()
//                .mapToObj(exp -> (Long) exp)
//                .flatMap(exp -> {
//                  //  LOG.info("Thread id in outer loop is {}", Thread.currentThread().getId());
//                    final int expNum = (int) (long) exp;
//                  //  final WiffReader tempLocalWiffReader = new WiffReader(inputFile);
//                    final MSExperiment experiment = localWiffReader.getDGivenIdx().getMassSepctrometerSample().GetMSExperiment(expNum);
//                    //final MSExperiment experiment = tempLocalWiffReader.getMassSepctrometerSample().GetMSExperiment(expNum);
//                    final MSExperimentInfo details = experiment.getDetails();
//                    //LOG.info("details.getDefaultResolution() {}, details.getExperimentName() {}, details.getSaturationThreshold() {}, details.getSourceType() {}, details.ToString() {}", details.getDefaultResolution(), details.getExperimentName(), details.getSaturationThreshold(), details.getSourceType(), details.ToString());
//                    final int scanCount = details.getNumberOfScans();
//               //     LOG.info("Number of scans is {}", scanCount);
//                    final IntStream scanCountStream = IntStream.range(0, scanCount);
//                    return scanCountStream
//                            .asLongStream()
//               //             .sequential()
//                            .parallel()
//                            .mapToObj(scanNum -> {
//                       //         final WiffReader tempLocalWiffReader2 = new WiffReader(inputFile);
//              //                  LOG.info("Thread id in inner loop is {}", Thread.currentThread().getId());
//                                return localWiffReader.getDGivenIdx().extractOneCentroidedSpectrum(expNum, (int) scanNum, baseline, minRtToProcess, maxRtToProcess, minMzToProcess, maxMzToProcess, true);
//                                //return tempLocalWiffReader2.extractOneCentroidedSpectrum(expNum, (int) scanNum, baseline, minRtToProcess, maxRtToProcess, minMzToProcess, maxMzToProcess);
//                            })
//                            .filter(rawDiaSpectrum -> rawDiaSpectrum != null);
//                });
//    }

    @Override
    public Stream<ImmutableDeconvolutedMzSpectrum> getDeconvolutedSpectrumStream(float minRtToProcess, float maxRtToProcess, double minMzToProcess, double maxMzToProcess, final float baseline, final int diaWindowToProcess, List<AtomicInteger> lstNumPeaksPerWindow, List<AtomicDouble> lstTotIntensityPerRtIntv, DoubleAccumulator maxDynamicRange, DoubleAccumulator maxI, DoubleAccumulator minI, List<AtomicDouble> lstNumPeaksPerRtIntv, LongAccumulator maxNumPeaksInSpectrum, AtomicInteger totalSpectra, DoubleAccumulator minMs1, DoubleAccumulator maxMs1, DoubleAccumulator minMs2, DoubleAccumulator maxMs2) {
        final int expNum = diaWindowToProcess + 1;
        if ((expNum < 1) || (expNum >= experimentCount))
            throw new RuntimeException("The diaWindowNum is out of bounds. It was " + diaWindowToProcess);
        //    final IntStream experimentCountStream = IntStream.range(minDiaWindowToProcess, maxDiaWindowToProcess + 1); //TODO for now, we're only pulling MS2 info; but we should change that....note: if we change that logic here, we should also adjust it in the mzXML extractor logic as well

        final AtomicInteger curScan = new AtomicInteger();
//        return experimentCountStream
//                .asLongStream()
////                .peek(exp -> LOG.info("ExpCount is {}", exp))
//                .parallel()
//                .mapToObj(exp -> (Long) exp)
//                .flatMap(exp -> {
//                    //  LOG.info("Thread id in outer loop is {}", Thread.currentThread().getId());
//                    final int expNum = (int) (long) exp;
        //for (int expNum=minDiaWindowToProcess; expNum <= maxDiaWindowToProcess; expNum++) {
        //  final WiffReader tempLocalWiffReader = new WiffReader(inputFile);
        final MSExperiment experiment = localWiffReader.get().getMassSepctrometerSample().GetMSExperiment(expNum);
        //final MSExperiment experiment = tempLocalWiffReader.getMassSepctrometerSample().GetMSExperiment(expNum);
        final MSExperimentInfo details = experiment.getDetails();
        //LOG.info("details.getDefaultResolution() {}, details.getExperimentName() {}, details.getSaturationThreshold() {}, details.getSourceType() {}, details.ToString() {}", details.getDefaultResolution(), details.getExperimentName(), details.getSaturationThreshold(), details.getSourceType(), details.ToString());
        final int scanCount = details.getNumberOfScans();
        //     LOG.info("Number of scans is {}", scanCount);
        final IntStream scanCountStream = IntStream.range(0, scanCount);
        return scanCountStream
                .asLongStream()
                //             .sequential()
                .parallel()
                .mapToObj(scanNum -> {
                    //         final WiffReader tempLocalWiffReader2 = new WiffReader(inputFile);
                    //                  LOG.info("Thread id in inner loop is {}", Thread.currentThread().getId());
                    final int curScanLocal = curScan.incrementAndGet();
                    if (curScanLocal % 500 == 0) {
                        LOG.info("Scan num {} of {}, so {} % complete.", curScanLocal, totalMs2Scans, (float) curScanLocal / (float) totalMs2Scans * 100f);
                    }
                    return localWiffReader.get().extractOneCentroidedSpectrum(expNum, (int) scanNum, baseline, minRtToProcess, maxRtToProcess, minMzToProcess, maxMzToProcess);
                    //return tempLocalWiffReader2.extractOneCentroidedSpectrum(expNum, (int) scanNum, baseline, minRtToProcess, maxRtToProcess, minMzToProcess, maxMzToProcess);
                })
                .filter(immutableDeconvolutedMzSpectrum -> immutableDeconvolutedMzSpectrum != null);
        //        }); //
    }

    @Override
    public MsHeaderInfo getMsHeaderInfo() {
        return sciexHeaderInfo;
    }
}
