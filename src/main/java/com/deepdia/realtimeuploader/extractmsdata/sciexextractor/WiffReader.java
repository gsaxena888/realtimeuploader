package com.deepdia.realtimeuploader.extractmsdata.sciexextractor;

import clearcore2.data.analystdataprovider.AnalystDataProviderFactory;
import clearcore2.data.analystdataprovider.AnalystWiffDataProvider;
import clearcore2.data.dataaccess.sampledata.Batch;
import clearcore2.data.dataaccess.sampledata.MSExperiment;
import clearcore2.data.dataaccess.sampledata.MassSpectrometerSample;
import clearcore2.data.dataaccess.sampledata.Sample;
import com.deepdia.realtimeuploader.common.misc.Constants;
import com.deepdia.realtimeuploader.extractmsdata.thermoextractor.DoThermoExtraction;
import it.unimi.dsi.fastutil.floats.Float2FloatAVLTreeMap;
import systems.deepsearch.common.pojo.ms.ImmutableDeconvolutedMzSpectrum;
import it.unimi.dsi.fastutil.doubles.Double2FloatAVLTreeMap;
import it.unimi.dsi.fastutil.doubles.DoubleArrayList;
import it.unimi.dsi.fastutil.floats.FloatArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WiffReader {
    private static final Logger LOG = LoggerFactory.getLogger(WiffReader.class);
    private final MassSpectrometerSample massSepctrometerSample;

    public WiffReader(String wiffFilepath) {
        if (LOG.isTraceEnabled()) LOG.trace("In init; thread id is {}", Thread.currentThread().getId());
        final AnalystWiffDataProvider provider = new AnalystWiffDataProvider();
        final Batch batch = AnalystDataProviderFactory.CreateBatch(wiffFilepath, provider);
        final Sample msSample = batch.GetSample(0);
        //  SampleInfo det = msSample.getDetails();
        this.massSepctrometerSample = msSample.getMassSpectrometerSample();
    }

    public static void determineIfWeShouldMerge(int expNum, float baseline, double minMzToProcess, double maxMzToProcess, float rt, Float2FloatAVLTreeMap mzAucIntensities, DoubleArrayList tempLstMz, FloatArrayList temLstI, double curMz, float curI, double nextMz) {
        if ((curMz >= minMzToProcess) && (curMz <= maxMzToProcess)) {
            if (LOG.isTraceEnabled() && curI > 100)
                LOG.trace("expNum {} and rt is {} mz {} intsi {}", expNum, rt, curMz, curI);
            tempLstMz.add(curMz);
            temLstI.add(curI);

            // final double nextMz = Double.MAX_VALUE;
            if ((nextMz - curMz) > (estimateFwhm(curMz, -Float.MAX_VALUE) * Constants.MAX_FRACTION_OF_FWHM_FOR_FEATURE_LINKING)) {

                final boolean merged = merge(tempLstMz, temLstI, estimateFwhm(curMz, -Float.MAX_VALUE) * Constants.MAX_FRACTION_OF_FWHM_FOR_FEATURE_LINKING);
                if (merged) {
                    if (curI > 100000)
                        if (DoThermoExtraction.numMerges.incrementAndGet() < 1000)
                            LOG.info("Merged on dia num {} rt {} and mz {} and nextMz {} and curI is {}", expNum, rt, curMz, nextMz, curI);
                }
                for (int j = 0; j < tempLstMz.size(); j++) {
                    if (temLstI.getFloat(j) >= baseline) {
                      //  DoThermoExtraction.numDpAfterMerges.incrementAndGet();
                        mzAucIntensities.put((float) tempLstMz.getDouble(j), temLstI.getFloat(j));
                    }
                }

                //reset accumulating vars
                tempLstMz.clear();
                temLstI.clear();
            }
        }
    }

    //OLD CODE for when were doing our own centroiding
//    public ImmutableRawDiaSpectrum extractOneCentroidedSpectrum(final int expNum, final int scanNum, final float baseline, final float minRtToPrcess, final float maxRtToProcess, final double minMzToProcess, final double maxMzToProcess, final boolean doVendorCentroiding) {
//        if (LOG.isTraceEnabled()) LOG.trace("In extract; thread id is {} and expNum is {} and scanNum is {}", Thread.currentThread().getId(), expNum, scanNum);
//        final MSExperiment experiment = massSepctrometerSample.GetMSExperiment(expNum);
//        final XYData spectrum = experiment.GetMassSpectrum(scanNum);
//        final float rt = (float) experiment.GetRTFromExperimentScanIndex(scanNum);
//        if ((rt < minRtToPrcess) || (rt > maxRtToProcess)) {
//            return null;
//        }
//
//        final double[] mz;
//        final double[] intensity;
//        if (doVendorCentroiding) {
//            final double[] centroidedDetails = experiment.GetCentroidedArray(scanNum);
//            final int doubleMsSize = centroidedDetails.length;
//            mz = new double[doubleMsSize / 2];
//            intensity = new double[doubleMsSize / 2];
//            for (int i = 0, j=0; i < doubleMsSize; i+=2, j++) {
//                mz[j] = centroidedDetails[i];
//                intensity[j] = centroidedDetails[i + 1];
//            }
////            final PeakClass[] peakArray = experiment.GetPeakArray(scanNum);
////            mz = new double[peakArray.length];
////            intensity = new double[peakArray.length];
////            for (int i = 0; i < peakArray.length; i++) {
////                mz[i] = peakArray[0].GetMz();
////                intensity[i] = peakArray[0].GetMz();
////                LOG.info("Mzi is {}", mz[i]);
////            }
//        }
//        else {
//            mz = spectrum.GetActualXValues();
//            intensity = spectrum.GetActualYValues();
//        }
//        final int numEdges = mz.length;
//
//        final DoubleArrayList mzList = new DoubleArrayList(numEdges);
//        final FloatArrayList intensityList = new FloatArrayList(numEdges);
//        for (int i = 0; i < numEdges; i++) {
//            final double curMz = mz[i];
//            final float curI = (float) intensity[i];
//            if ((minMzToProcess <= curMz) && (curMz <= maxMzToProcess) && (curI > baseline)) {
//                mzList.add(curMz);
//                intensityList.add(curI);
//            }
//        }
//        mzList.trim();
//        intensityList.trim();
//        final ImmutableRawDiaSpectrum rawSpectrum = ImmutableRawDiaSpectrum
//                .builder()
//                .setRtInMinutes(rt)
//                .setDiaWindowNum(expNum - 1)
//                .setMzs(mzList)
//                .setIntensities(intensityList)
//                .setNumRegions(new NumRegionsHackWrapper())
//                .build();
//        return rawSpectrum;
//    }

    public static double estimateFwhm(double mz, float intensity) {
        //FOR now, we've hardcoded this for sciex intstruments
//        final double resolution = 15000;
//        final double fwhm = mz / resolution;
//        return fwhm;

        //TODO remove hardcoding for thermo
        final float ppmTol = 50f;
        return (ppmTol * mz) / (1e6 * Constants.MAX_FRACTION_OF_FWHM_FOR_FEATURE_LINKING);
    }

    private static boolean merge(DoubleArrayList tempLstMz, FloatArrayList temLstI, double tolMz) {
        //find index of min pair distance
        boolean merged = false;
        double minDiffMzSoFar = Double.MAX_VALUE;
        do {
            //intiialize vars
            minDiffMzSoFar = Double.MAX_VALUE;
            int minIidx = Integer.MIN_VALUE;

            //main loop
            for (int i = 0; i < tempLstMz.size() - 1; i++) {
                final double curDiffMz = tempLstMz.getDouble(i + 1) - tempLstMz.getDouble(i);
                if (curDiffMz < minDiffMzSoFar) {
                    minIidx = i;
                    minDiffMzSoFar = curDiffMz;
                }
            }

            //now that index is found, merge them if the diff is less than tolMz
            if (minDiffMzSoFar <= tolMz) {
                merged = true;
              //  DoThermoExtraction.numMerges.incrementAndGet();
                final float newIntensity = (temLstI.getFloat(minIidx) + temLstI.getFloat(minIidx + 1));
                final double newMz = ((tempLstMz.getDouble(minIidx) * temLstI.getFloat(minIidx)) + (tempLstMz.getDouble(minIidx + 1) * temLstI.getFloat(minIidx + 1))) / newIntensity;
                tempLstMz.set(minIidx, newMz);
                temLstI.set(minIidx, newIntensity);
                tempLstMz.removeElements(minIidx + 1, minIidx + 2);
                temLstI.removeElements(minIidx + 1, minIidx + 2);
            }
        } while (minDiffMzSoFar <= tolMz);
        return merged;
    }

    public MassSpectrometerSample getMassSepctrometerSample() {
        return massSepctrometerSample;
    }

    public ImmutableDeconvolutedMzSpectrum extractOneCentroidedSpectrum(final int expNum, final int scanNum, final float baseline, final float minRtToPrcess, final float maxRtToProcess, final double minMzToProcess, final double maxMzToProcess) {
        if (LOG.isTraceEnabled())
            LOG.trace("In extract; thread id is {} and expNum is {} and scanNum is {}", Thread.currentThread().getId(), expNum, scanNum);
        final MSExperiment experiment = massSepctrometerSample.GetMSExperiment(expNum);
        final float rt = (float) experiment.GetRTFromExperimentScanIndex(scanNum);
        if ((rt < minRtToPrcess) || (rt > maxRtToProcess)) {
            return null;
        }

        final Float2FloatAVLTreeMap mzAucIntensities = new Float2FloatAVLTreeMap();
        final double[] centroidedDetails = experiment.GetCentroidedArray(scanNum); //TODO add baseline, minMzToProcess, maxMzToProcess
        final int doubleMsSize = centroidedDetails.length;

        //initial accumulating vars declaration
        final DoubleArrayList tempLstMz = new DoubleArrayList(5);
        final FloatArrayList temLstI = new FloatArrayList(5);

        //main loop
        for (int i = 0; i < doubleMsSize; i += 2) {
            final double curMz = centroidedDetails[i];
            final float curI = (float) centroidedDetails[i + 1];
            final double nextMz = ((i + 2) < doubleMsSize) ? centroidedDetails[i + 2] : Double.MAX_VALUE;
            determineIfWeShouldMerge(expNum, baseline, minMzToProcess, maxMzToProcess, rt, mzAucIntensities, tempLstMz, temLstI, curMz, curI, nextMz);
        }

        return ImmutableDeconvolutedMzSpectrum
                .builder()
                .setDIAWindowNum(expNum - 1)
                .setRtInMinutes(rt)
                .setMzAUCIntensities(mzAucIntensities)
                .build();
    }
}
