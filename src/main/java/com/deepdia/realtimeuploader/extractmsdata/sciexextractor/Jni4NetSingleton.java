package com.deepdia.realtimeuploader.extractmsdata.sciexextractor;

import com.deepdia.realtimeuploader.common.misc.Utilities;
import net.sf.jni4net.Bridge;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

public class Jni4NetSingleton {
    private static final Logger LOG = LoggerFactory.getLogger(Jni4NetSingleton.class);
    private static Jni4NetSingleton ourInstance = new Jni4NetSingleton();
    private Set<String> loadedDlls;

    private Jni4NetSingleton() {
        Bridge.setVerbose(false);
        try {
            final File directoryOfExecutableJar = Utilities.getDirectoryOfExecutableJar();
            Bridge.init(directoryOfExecutableJar);
        } catch (Throwable e) {
            throw new RuntimeException(e);
        }
        loadedDlls = new HashSet<>();
    }

    public static Jni4NetSingleton getInstance() {
        return ourInstance;
    }

    public synchronized void loadDll(String dllName) {
        //Bridge.LoadAndRegisterAssemblyFrom(new File("sciexextractor.j4n.dll"));
        if (!loadedDlls.contains(dllName)) {
            Bridge.LoadAndRegisterAssemblyFrom(new File(dllName));
            loadedDlls.add(dllName);
            LOG.info("Loaded dll of {}", dllName);
        }
    }
}
