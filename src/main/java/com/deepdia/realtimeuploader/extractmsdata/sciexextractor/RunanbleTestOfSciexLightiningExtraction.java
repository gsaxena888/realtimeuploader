//package com.deepdia.deepsearch.modules.inputfileextractors.sciexextractor;
//
//import clearcore2.data.dataaccess.sampledata.*;
//import clearcore2.licensing.LicenseKeys;
//import ImmutableRawDiaSpectrum;
//import it.unimi.dsi.fastutil.floats.FloatArrayList;
//import net.sf.jni4net.Bridge;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import java.io.File;
//import java.io.IOException;
//import java.util.List;
//import java.util.stream.Collectors;
//import java.util.stream.IntStream;
//import java.util.stream.Stream;
//
//public class RunanbleTestOfSciexLightiningExtraction {
//    private static String[] ABI_BETA_LICENSE_KEY = new String[] {"<?xml version=\"1.0\" encoding=\"utf-8\"?><license_key><company_name>Proteo Wizard|Redistributable Beta Agreemeent 2012-03-20</company_name><product_name>ProcessingFramework</product_name><features>WiffReaderSDK</features><key_data>Z9360tXYtYTi+Ru6cbynF52hEBoKTFNHT0vYGPCI5ZmmAXRZ+W26Ag==</key_data></license_key>"};
//    private static final Logger LOG = LoggerFactory.getLogger(RunanbleTestOfSciexLightiningExtraction.class);
//
//    public static void main(String arsg[]) throws IOException {
//        initJ4N();
//
//        final String wiffFilepath = "c:/Users/gsaxena888/Downloads/Plasma_001_s002_r1.wiff";
//        final ThreadLocal<WiffReader> localWiffReader = ThreadLocal.withInitial(() -> new WiffReader(wiffFilepath));
//        //  LOG.info("det.getAcquisitionMethodName() {}, det.getAcquisitionDateTime() {},  det.getDADDetectorName() {}, det.getDilutionFactor() {}, det.getExtraProperties() {}, det.getInjectionVolume() {}, det.getInstrumentName() {}, det.getInstrumentSerialNumber() {}, det.getNumCustomFields() {}, det.getNumPeakConcentrations() {}, det.getBatchName() {}, det.getDilutionFactor() {}, det.getAcquisitionMethodName() {}, det.getPlate() {}, det.getRack() {}, det.getSampleComment() {}, det.getSampleID() {}, det.getSampleName() {}, det.getInstrumentSerialNumber() {}, det.getInjectionVolume() {}, det.getSampleType() {}, det.getUserName() {}, det.getVial() {}", det.getAcquisitionMethodName(), det.getAcquisitionDateTime(), det.getDADDetectorName(), det.getDilutionFactor(), det.getExtraProperties(), det.getInjectionVolume(), det.getInstrumentName(), det.getInstrumentSerialNumber(), det.getNumCustomFields(), det.getNumPeakConcentrations(), det.getBatchName(), det.getDilutionFactor(), det.getAcquisitionMethodName(), det.getPlate(), det.getRack(), det.getSampleComment(), det.getSampleID(), det.getSampleName(), det.getInstrumentSerialNumber(), det.getInjectionVolume(), det.getSampleType(), det.getUserName(), det.getVial());
//        final int experimentCount = localWiffReader.getDGivenIdx().getMassSepctrometerSample().getExperimentCount();
//        LOG.info("There are {} MS1 and MS2 experiments/windows", experimentCount);
//        final IntStream experimentCountStream = IntStream.range(0, experimentCount);
//        experimentCountStream
//                .asLongStream()
//            //    .sequential()
//                .parallel()
//                .forEach(exp -> {
//                    final int expNum = (int) exp;
//                    final MSExperiment experiment = localWiffReader.getDGivenIdx().getMassSepctrometerSample().GetMSExperiment(expNum);
//                    final MSExperimentInfo details = experiment.getDetails();
//                    //LOG.info("details.getDefaultResolution() {}, details.getExperimentName() {}, details.getSaturationThreshold() {}, details.getSourceType() {}, details.ToString() {}", details.getDefaultResolution(), details.getExperimentName(), details.getSaturationThreshold(), details.getSourceType(), details.ToString());
//                    final int scanCount = details.getNumberOfScans();
//                    final ExperimentType expType = details.getExperimentType();
//                    final boolean isProductScan =  expType.ToString().equals("Product");
//                    if (isProductScan) {
//                        MassRange[] massRangeInfo = details.getMassRangeInfo();
//                        final FragmentBasedScanMassRange fragmentBasedScanMassRange = (FragmentBasedScanMassRange) massRangeInfo[0];
//                        final double centerMz = fragmentBasedScanMassRange.getFixedMasses()[0];
//                        final double width = fragmentBasedScanMassRange.getIsolationWindow();
//                        final double startMz = centerMz - (width / 2d);
//                        final double endingMz = centerMz + (width / 2d);
//                   //     LOG.info("Exp start {} and end {}", startMz, endingMz);
//                        //LOG.info("MR dwelltime {} and {} and sm {} and em {}", massRangeInfo[0].getDwellTime(), massRangeInfo[0].getName(), details.getStartMass(), details.getEndMass());
//                    }
//
//                    final IntStream scanCountStream = IntStream.range(0, scanCount);
//                    final Stream<ImmutableRawDiaSpectrum> spectraParallelStream = scanCountStream
//                            .asLongStream()
//                            .parallel()
//                            .mapToObj(scanNum -> localWiffReader.getDGivenIdx().extractOneCentroidedSpectrum(expNum, (int) scanNum, 0, 0, Float.MAX_VALUE, 0, Double.MAX_VALUE));
//
//                    final List<ImmutableRawDiaSpectrum> dummyLst = spectraParallelStream.collect(Collectors.toList());
//                    LOG.info("DummyLst numEdges is {} and first list's first mz is {}", dummyLst.numEdges(), dummyLst.getDGivenIdx(0).getRtInMinutes());
//                });
//
//        }
//
//
//    private static void initJ4N() throws IOException {
//        Bridge.setVerbose(false);
//        Bridge.init(new File(System.getProperty("user.dir")));
//        Bridge.LoadAndRegisterAssemblyFrom(new File("sciexextractor.j4n.dll"));
//
//        LicenseKeys.setKeys(ABI_BETA_LICENSE_KEY);
//    }
//}
//
//
