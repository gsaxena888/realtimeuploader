package com.deepdia.realtimeuploader.extractmsdata;

import org.immutables.value.Value;


/**
 * @author gautam
 */
@Value.Immutable(builder = false)
@Value.Style(init = "set*")
public abstract class MzIntensity {

    // vars
    @Value.Parameter
    public abstract double getMz();

    @Value.Parameter
    public abstract float getIntensity();
}

