package com.deepdia.realtimeuploader.extractmsdata;

import com.deepdia.realtimeuploader.extractmsdata.mzxmlextractor.entities.NumRegionsHackWrapper;
import com.google.common.collect.ComparisonChain;
import it.unimi.dsi.fastutil.doubles.DoubleArrayList;
import it.unimi.dsi.fastutil.doubles.DoubleListIterator;
import it.unimi.dsi.fastutil.floats.FloatArrayList;
import it.unimi.dsi.fastutil.floats.FloatListIterator;
import org.immutables.value.Value;

import java.io.Serializable;
import java.util.Iterator;

@Value.Immutable
@Value.Style(init = "set*", stagedBuilder = true, strictBuilder = true)
public abstract class RawDiaSpectrum implements Comparable<RawDiaSpectrum>, Iterable<ImmutableMzIntensity>, Serializable {
    private static final long serialVersionUID = 1L;

    public abstract float getRtInMinutes(); //this is what makes it unique

    @Value.Auxiliary
    public abstract int getDiaWindowNum();

    @Value.Auxiliary
    public abstract DoubleArrayList getMzs();

    @Value.Auxiliary
    public abstract FloatArrayList getIntensities(); //it's a double here only because the base64 encoding brings it in as double (unfortunately) -- eventually, we convert it to float....longer term, we could convert it to a short and do all logic using shorts intead of floats

    @Value.Auxiliary
    public abstract NumRegionsHackWrapper getNumRegions();

    @Value.Auxiliary
    public abstract ModifiableStartIdx getModifiableStartIdx();


    // ************************************************
    // COMPARATOR
    // ************************************************
    @Override
    public int compareTo(RawDiaSpectrum o) {
        return ComparisonChain.start()
                .compare(getDiaWindowNum(), o.getDiaWindowNum())
                .compare(getRtInMinutes(), o.getRtInMinutes())
                .result();
    }

    @Override
    public Iterator<ImmutableMzIntensity> iterator() {
        return new Iterator<ImmutableMzIntensity>() {
            DoubleListIterator itrMzs = getMzs().iterator();
            FloatListIterator itrInt = getIntensities().iterator();

            @Override
            public boolean hasNext() {
                return itrMzs.hasNext();
            }

            @Override
            public ImmutableMzIntensity next() {
                return ImmutableMzIntensity.of(itrMzs.nextDouble(), (float) itrInt.nextFloat());
            }
        };
    }

}
