package com.deepdia.realtimeuploader.extractmsdata;

import com.google.common.collect.ImmutableMap;
import systems.deepsearch.common.pojo.ms.embedded.ImmutableDIAWindowHeader;


import java.util.HashMap;

public interface MsHeaderInfo {
    public String getFileName();

    //    public  long getFileSize();
//
    public String getMsManufacturer();

    public String getMsModel();

//    public  float getFirsRtMinutesOverAllWindows();
//
//   public  float getLastRtMinutesOverAllWindows();
//
//   public  double getMinMzForMs2();
//
//   public  double getMaxMzForMs2();

//    public  int getNumScans();

    public HashMap<Integer, ImmutableDIAWindowHeader> getAllDIAWindowHeadersByDiaWindowNum();

    public ImmutableDIAWindowHeader getOneDiaWindowHeaderByDiaWindowNum(int diaWindowNum);

    public int getNumDIAWindows();

}
