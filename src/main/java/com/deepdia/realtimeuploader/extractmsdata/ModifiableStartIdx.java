package com.deepdia.realtimeuploader.extractmsdata;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Objects;

public class ModifiableStartIdx implements Serializable { //TODO rename to StartIdxWrapper and just comment about reason for name (ie to be modificable)
    private static final Logger LOG = LoggerFactory.getLogger(ModifiableStartIdx.class);

    private int startIdx;

    public int getStartIdx() {
        return startIdx;
    }

    public void setStartIdx(int startIdx) {
        this.startIdx = startIdx;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ModifiableStartIdx that = (ModifiableStartIdx) o;
        return startIdx == that.startIdx;
    }

    @Override
    public int hashCode() {

        return Objects.hash(startIdx);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ModifiableStartIdx{");
        sb.append("startIdx=").append(startIdx);
        sb.append('}');
        return sb.toString();
    }
}
