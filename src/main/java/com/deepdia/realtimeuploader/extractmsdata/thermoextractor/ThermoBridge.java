//package com.deepdia.deepsearch.modules.inputfileextractors.thermoextractor;
//
//import com.javonet.Javonet;
//import com.javonet.JavonetException;
//import com.javonet.JavonetFramework;
//import com.javonet.api.NEnum;
//import com.javonet.api.NException;
//import com.javonet.api.NObject;
//import com.javonet.api.NType;
//
//import java.time.Duration;
//import java.time.Instant;
//import java.util.List;
//import java.util.concurrent.ConcurrentHashMap;
//import java.util.concurrent.atomic.AtomicInteger;
//import java.util.concurrent.atomic.AtomicLong;
//import java.util.function.Consumer;
//import java.util.stream.Collectors;
//import java.util.stream.IntStream;
//
//public class ThermoBridge {
//
//	public static final <I> void forp(final List<I> inputList, final Consumer<I> body) {
//	    inputList.parallelStream().forEach(body);
//	}
//
//	int lastScan;
//	AtomicInteger enumeratedSpectra;
//	AtomicLong  totalDataPoints;
//	AtomicInteger  precursorCount;
//	ConcurrentHashMap<Long, NObject> accessorByThread;
//
//	public void getSpectrum(NObject raw, Integer scanIndex, boolean centroid) throws Exceptions
//	{
//		//System.out.format("%d\n", scanIndex);
//
//    	NObject spectrum = centroid ? raw.invoke("GetSimplifiedCentroids", scanIndex) : raw.invoke("GetSimplifiedScan", scanIndex);
//
//
//    	int enumerated = enumeratedSpectra.incrementAndGet();
//    	if ((enumerated % 100)==0)
//			System.out.format("%d/%d\n", enumerated, lastScan);
//
//
//    	double[] mz = spectrum.getDGivenIdx("Masses");
//    	double[] intensity = spectrum.getDGivenIdx("Intensities");
//    	if (mz.length != intensity.length)
//    		throw new Exceptions();
//    	totalDataPoints.addAndGet(mz.length);
//
//    	double rt = raw.invoke("RetentionTimeFromScanNumber", scanIndex);
//
//    	NObject filter = raw.invoke("GetFilterForScanNumber", scanIndex);
//
//    	int msOrder = filter.<NEnum>getDGivenIdx("MSOrder").getValue();
//    	double precursorMz = 0;
//    	if (msOrder > 1) {
//			precursorMz = filter.invoke("GetMass", 0);
//			System.out.println("Precursor Mz is " + precursorMz + " " + "GetFirstPrecursorMass Mz is " + filter.invoke("GetFirstPrecursorMass", 0) + " " + "GetLastPrecursorMass Mz is " + filter.invoke("GetLastPrecursorMass", 0) + " " + "GetIsolationWidth Mz is " + filter.invoke("GetIsolationWidth", 0));
//		}
//    	if (precursorMz > 0)
//    		precursorCount.incrementAndGet();
//	}
//
//	public void run()
//	{
//		accessorByThread = new ConcurrentHashMap<>();
//		enumeratedSpectra = new AtomicInteger();
//		totalDataPoints = new AtomicLong();
//		precursorCount = new AtomicInteger();
//
//		try
//		{
//		//	Javonet.activate("gsaxena888@gmail.com", "r3E7-t7X3-c4HA-n3TM-n8B4", JavonetFramework.v40);
//			Javonet.activate("gsaxena@i-a-inc.com", "Yw35-Xw7d-k8QM-Cw38-y8S5", JavonetFramework.v45);
//			Javonet.setUsePrimitiveArrays(true);
//			Javonet.addReference("ThermoFisher.CommonCore.RawFileReader.dll");
//			Javonet.addReference("ThermoFisher.CommonCore.Data.dll");
//			Javonet.addReference("ThermoFisher.CommonCore.BackgroundSubtraction.dll");
//			Javonet.addReference("ThermoFisher.CommonCore.MassPrecisionEstimator.dll");
//
//		    String filepath = "C:\\Users\\gsaxena888\\Downloads\\DeepSearchMarch30\\samplefiles\\thermo\\01_c01_br01_tr01.raw";
//
//		    NType rawFileReaderAdapter = Javonet.getType("ThermoFisher.CommonCore.RawFileReader.RawFileReaderAdapter");
//		    NObject rawThreaded = rawFileReaderAdapter.invoke("ThreadedFileFactory", filepath);
//		    NObject raw = rawThreaded.invoke("CreateThreadAccessor");
//		    raw.invoke("SelectInstrument", 0, 1);
//		    NObject header = raw.getDGivenIdx("RunHeader");
//		    int firstScan = header.getDGivenIdx("FirstSpectrum");
//		    lastScan = header.getDGivenIdx("LastSpectrum");
//
//	        Instant start = Instant.now();
//
//        	// Parallel enumeration
//        	List<Integer> scanIndexList = IntStream.rangeClosed(firstScan, lastScan).boxed().collect(Collectors.toList());
//        	forp(scanIndexList, (c) -> {
//				try {
//					long threadId = Thread.currentThread().getId();
//					NObject rawp = accessorByThread.computeIfAbsent(threadId, (i) -> {
//						try {
//							NObject a = rawThreaded.invoke("CreateThreadAccessor");
//							a.invoke("SelectInstrument", 0, 1);
//							return a;
//						} catch (JavonetException e) {
//							e.printStackTrace();
//						}
//					return null; });
//					this.getSpectrum(rawp, c, true);
//				} catch (JavonetException e) {
//					if (e instanceof NException) {
//						NException inner = ((NException) e).getInnerException();
//						if (inner != null) {
//							System.err.println(inner.getExceptionTypeName());
//							inner.printStackTrace();
//						}
//					} else
//						e.printStackTrace();
//					System.exit(1);
//				} catch (Exceptions e) {
//					e.printStackTrace();
//					System.exit(1);
//				}
//			});
//
//        	// Serial enumeration: slow
//        	/*for (int scanIndex = firstScan; scanIndex <= lastScan; ++scanIndex)
//        	{
//	        	NObject spectrum = raw.invoke("GetSimplifiedScan", scanIndex);
//
//	        	Double[] mz = spectrum.getDGivenIdx("Masses");
//	        	Double[] intensity = spectrum.getDGivenIdx("Intensities");
//	        	if (mz.length != intensity.length)
//	        		throw new Exceptions();
//	        	totalDataPoints.addAndGet(mz.length);
//
//	        	double rt = raw.invoke("RetentionTimeFromScanNumber", scanIndex);
//
//	        	NObject filter = raw.invoke("GetFilterForScanNumber", scanIndex);
//
//	        	int msOrder = filter.<NEnum>getDGivenIdx("MSOrder").getValue();
//	        	double precursorMz = 0;
//	        	if (msOrder > 1)
//	        		precursorMz = filter.invoke("GetMass", 0);
//	        	if (precursorMz > 0)
//	        		precursorCount.incrementAndGet();
//
//	        	//System.out.format("%s      (%f, %f) - (%f, %f)   %f   %f   %s\n", experimentType.getValueName(), mz[0], intensity[0], mz[mz.length-1], intensity[intensity.length-1], rt, parentMz, isolationWidth);
//	        	//if (!isProductScan)
//	        	//	break;
//        	}*/
//	        Instant stop = Instant.now();
//	        System.out.format("Total datapoints: %d, precursor count: %d, time elapsed: %s", totalDataPoints.getDGivenIdx(), precursorCount.getDGivenIdx(), Duration.between(start, stop));
//
//		} catch (JavonetException e) {
//			if (e instanceof NException) {
//				NException inner = ((NException) e).getInnerException();
//				if (inner != null) {
//					System.err.println(inner.getExceptionTypeName());
//					inner.printStackTrace();
//				}
//			} else
//				e.printStackTrace();
//		} catch (Exceptions e) {
//			e.printStackTrace();
//		}
//	}
//
//	public static void main(String[] args)
//	{
//		ThermoBridge test = new ThermoBridge();
//		test.run();
//	}
//}
