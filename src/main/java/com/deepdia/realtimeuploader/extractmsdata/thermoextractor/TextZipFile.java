package com.deepdia.realtimeuploader.extractmsdata.thermoextractor;

import com.deepdia.realtimeuploader.ImportMsFile;
import com.deepdia.realtimeuploader.MyUtils;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import net.lingala.zip4j.ZipFile;
import net.lingala.zip4j.model.FileHeader;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.ParseException;
import systems.deepsearch.common.pojo.ms.ImmutableDeconvolutedMzSpectrum;
import systems.deepsearch.common.util.ProgressCounter;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.deepdia.realtimeuploader.ImportMsFile.OUTPUT_FOLDER;

@Slf4j
public class TextZipFile {

    public static final String testFileName = "1B26_Binek_DIA_HFpEF_cyto_1009.raw";

    @SneakyThrows
    public static void main(String args[])  {
        final String fileNameOfZippedFile = OUTPUT_FOLDER + testFileName + ".ds";
        final ZipFile zipFile = new ZipFile(fileNameOfZippedFile);
        final List<FileHeader> lstHeaders = zipFile.getFileHeaders();
        lstHeaders.stream().map(e -> e.getFileName()).forEach(e -> log.warn("FileName is {}", e));
        FileHeader fileHeader = zipFile.getFileHeader(lstHeaders.get(0).getFileName());
        InputStream inputStream = zipFile.getInputStream(fileHeader);

        ArrayList<ImmutableDeconvolutedMzSpectrum> des = MyUtils.deserializeInputStream(ArrayList.class, inputStream);
        log.warn("DiaNumis {}", des.get(0).getDIAWindowNum());
    }
}
