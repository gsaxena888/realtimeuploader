package com.deepdia.realtimeuploader.extractmsdata.thermoextractor;

import com.deepdia.realtimeuploader.ImportMsFile;
import com.deepdia.realtimeuploader.extractmsdata.*;
import com.google.common.math.DoubleMath;
import com.google.common.util.concurrent.AtomicDouble;
import it.unimi.dsi.fastutil.floats.Float2FloatAVLTreeMap;
import systems.deepsearch.common.pojo.ms.embedded.ImmutableDIAWindowHeader;
import com.javonet.Javonet;
import com.javonet.JavonetFramework;
import com.javonet.api.NEnum;
import com.javonet.api.NObject;
import com.javonet.api.NType;
import it.unimi.dsi.fastutil.doubles.DoubleArrayList;
import it.unimi.dsi.fastutil.floats.FloatArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import systems.deepsearch.common.pojo.ms.ImmutableDeconvolutedMzSpectrum;

import java.io.File;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.DoubleAccumulator;
import java.util.concurrent.atomic.LongAccumulator;
import java.util.stream.IntStream;
import java.util.stream.Stream;

//import thermofisher.commoncore.data.interfaces.IRawFileThreadManager;

public class DoThermoExtraction implements InputFileExtractor {
    private static final Logger LOG = LoggerFactory.getLogger(DoThermoExtraction.class);
    private static final int FIRST_SCANNUM_FOR_MS2 = 2;
    // public static AtomicInteger numDpBeforeMerges = new AtomicInteger(0);
    // public static AtomicInteger numDpAfterMerges = new AtomicInteger(0);
    public static AtomicInteger numMerges = new AtomicInteger(0);
    public static String DLL_PATH = "";
    static {
        try {
            LOG.info("Starting Javonet config");
            //Javonet.activate("gsaxena@i-a-inc.com", "Yw35-Xw7d-k8QM-Cw38-y8S5", JavonetFramework.v45);
            //      Javonet.activate("info@i-a-inc.com", "s9XN-p4B9-Ej6e-Zm93-Xj97", JavonetFramework.v45);
            File file = new File(System.getenv("AllUsersProfile") + System.getProperty("file.separator") + "Deepsearch");
            if (!file.exists()) {
                file.mkdirs();
            }
            DLL_PATH = System.getProperty("deepsearch_home");
            LOG.info("DLL_PATH = " + DLL_PATH);

            Javonet.setLicenseDirectory(DLL_PATH);
            //Javonet.activate("mboutaskiouine@gmail.com", "Po6m-o2Y4-j8N9-z7K2-Sp4s", JavonetFramework.v45);
            //Javonet.activate("gautam.saxena@deepsearch.systems", "Pz9o-Rg3y-s8J2-a7S2-a8SA", JavonetFramework.v45);
        //    Javonet.activate("mb.outaskiouine@gmail.com", "Rx8p-i3EC-Li9t-Cq8b-Yb5d", JavonetFramework.v45);
            Javonet.activate("gsaxena@root-cause.ai", "Nx8t-w4SJ-s2Q5-d2FS-Sg35", JavonetFramework.v45);

            Javonet.setUsePrimitiveArrays(true);
            Javonet.addReference(DLL_PATH + File.separator + "ThermoFisher.CommonCore.RawFileReader.dll");
            Javonet.addReference(DLL_PATH + File.separator + "ThermoFisher.CommonCore.Data.dll");
            Javonet.addReference(DLL_PATH + File.separator + "ThermoFisher.CommonCore.BackgroundSubtraction.dll");
            Javonet.addReference(DLL_PATH + File.separator + "ThermoFisher.CommonCore.MassPrecisionEstimator.dll");
        } catch (Exception e) {
            LOG.error(e.getMessage());
            throw new RuntimeException(e);
        }
        LOG.info("Finished Javonet config");
    }

    private final int MAX_DIA_WINDOWS = 300; //we never expect to see more than 300 windows, so this is a safe maximum such that if we look at the first 300 scans, we should getDGivenIdx all the needed DIA windows....
    private final boolean centroid = true;
    //    private final int experimentCount;
    private final ThreadLocal<NObject> rawThreadAccessorLocalThread;
    private final ImmutableThermoHeaderInfo thermoHeaderInfo;
    private final int firstScan;
    private final int lastScan;


    public DoThermoExtraction(File inputFile) {
        LOG.info("Starting Thermo Extraction");
        if (!inputFile.exists())
            throw new RuntimeException("The input file " + inputFile.getAbsolutePath() + " does not exist!");

        //load the Sciex dll
        try { //TODO technially, we should do this elsewhere so it's done only once on program startup, eg in Utilities etc.


            //String filepath = "C:\\Users\\gsaxena888\\Downloads\\DeepSearchMarch30\\samplefiles\\thermo\\01_c01_br01_tr01.rawThreadAccessor";


            rawThreadAccessorLocalThread = ThreadLocal.withInitial(() -> {
                try {
                    //      System.out.println("Creating rawThreaded in threadid {}", Thread.currentThread().getId());
                    final NType rawFileReaderAdapter = Javonet.getType("ThermoFisher.CommonCore.RawFileReader.RawFileReaderAdapter");
                    final NObject rawThreaded = rawFileReaderAdapter.invoke("ThreadedFileFactory", inputFile.getAbsolutePath());
                    final NObject threadAccessor = rawThreaded.invoke("CreateThreadAccessor");
                    threadAccessor.invoke("SelectInstrument", 0, 1);
                    //   System.out.println("Done Creating rawThreaded in threadid {}", Thread.currentThread().getId());
                    return threadAccessor;
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            });
            //   rawThreadAccessorLocalThread.getDGivenIdx().invoke("SelectInstrument", 0, 1);
            final NObject header = rawThreadAccessorLocalThread.get().get("RunHeader");
            firstScan = header.get("FirstSpectrum");
            lastScan = header.get("LastSpectrum");

            final HashMap<Integer, ImmutableDIAWindowHeader> diaWindowHeaderMap = new HashMap<>(128);
            for (int scanIndex = FIRST_SCANNUM_FOR_MS2; scanIndex < MAX_DIA_WINDOWS; scanIndex++) {
                //final NObject spectrum = centroid ? rawThreadAccessor.invoke("GetSimplifiedCentroids", scanIndex) : rawThreadAccessor.invoke("GetSimplifiedScan", scanIndex);
//                double[] mz = spectrum.getDGivenIdx("Masses");
//                double[] intensity = spectrum.getDGivenIdx("Intensities");
//                if (mz.length != intensity.length)
//                    throw new Exceptions();
//                totalDataPoints.addAndGet(mz.length);

//
                final NObject filter = rawThreadAccessorLocalThread.get().invoke("GetFilterForScanNumber", scanIndex);
                final int msOrder = filter.<NEnum>get("MSOrder").getValue();

                if (msOrder == 1) break; // we reached the next MS1 entry, so we're done.
                else {//(msOrder == 2) {
                    final int diaWindowNum = scanIndex - FIRST_SCANNUM_FOR_MS2;
                    final double diaWindowMzCenter = filter.invoke("GetMass", 0);
                    final double diaWindowMzWidth = filter.invoke("GetIsolationWidth", 0);
                    diaWindowHeaderMap.put(diaWindowNum,
                            new ImmutableDIAWindowHeader(diaWindowNum, diaWindowMzCenter, diaWindowMzWidth, (float) (double) rawThreadAccessorLocalThread.get().invoke("RetentionTimeFromScanNumber", scanIndex))
                    );
                }
            }

            thermoHeaderInfo = ImmutableThermoHeaderInfo
                    .builder()
                    .setFileName(inputFile.getAbsolutePath())
                    .setMsManufacturer("Thermo") //todo verify the exact name used in the mzxml files and use that one only (inlcuidng case sensitivitiy)
                    .setMsModel("TBD") //todo getDGivenIdx this value from actual wiff file and also make sure it matches mzXML file settings.
                    .setAllDIAWindowHeadersByDiaWindowNum(diaWindowHeaderMap)
                    .setNumDIAWindows(diaWindowHeaderMap.size())
                    .build();

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    @Override
    public Stream<ImmutableRawDiaSpectrum> getRawSpectrumStream(int minDiaWindowToProcess, int maxDiaWindowToProcess, float minRtToProcess, float maxRtToProcess, double minMzToProcess, double maxMzToProcess, final float baseline) {
        throw new RuntimeException("No longer supported"); // we only now support getting centroided data out; later, if useful, we can revist this...
    }


    @Override
    public Stream<ImmutableDeconvolutedMzSpectrum> getDeconvolutedSpectrumStream(float minRtToProcess, float maxRtToProcess, double minMzToProcess, double maxMzToProcess, final float baseline, final int diaWindowToProcess, List<AtomicInteger> lstNumPeaksPerWindow, List<AtomicDouble> lstTotIntensityPerRtIntv, DoubleAccumulator maxDynamicRange, DoubleAccumulator maxI, DoubleAccumulator minI, List<AtomicDouble> lstNumPeaksPerRtIntv, LongAccumulator maxNumPeaksInSpectrum, AtomicInteger totalSpectra, DoubleAccumulator minMs1, DoubleAccumulator maxMs1, DoubleAccumulator minMs2, DoubleAccumulator maxMs2) {
        final int numScansInCycle = getMsHeaderInfo().getNumDIAWindows() + 1; //we add one for the MS1 scan
        final IntStream.Builder scanRangeBuilder = IntStream.builder();
        int totalMs2ScansCounter = 0;
        for (int scanIndex = FIRST_SCANNUM_FOR_MS2 + diaWindowToProcess; scanIndex <= lastScan; scanIndex += numScansInCycle) {
            scanRangeBuilder.accept(scanIndex);
            totalMs2ScansCounter++;
        }
        final int totalMs2Scans = totalMs2ScansCounter;
        totalSpectra.addAndGet(totalMs2Scans);
        final IntStream scansToProcess = scanRangeBuilder.build();
        final AtomicInteger curScan = new AtomicInteger();
        final double[] prevPrecursorMz = {-Double.MAX_VALUE};

        return scansToProcess
                .asLongStream()
                //.sequential()
                .parallel()
                .mapToObj(scanIndex -> scanIndex)
                .map(scanIndexLong -> {
                    final int curScanLocal = curScan.incrementAndGet();
                    if (curScanLocal % 512 == 0) {
                        //System.out.println("Scan num {} of {}, so {} % complete. With precursormz of {}", curScanLocal, totalMs2Scans, (float) curScanLocal / (float) totalMs2Scans * 100f, prevPrecursorMz[0]);
                    }
                    final int scanIndex = (int) (long) scanIndexLong;

                    try {
                        // System.out.println("About to extract rt for scan num {} with id {} and rawThread of {}", scanIndex, Thread.currentThread().getId(),  rawThreadAccessorLocalThread.getDGivenIdx().toString());
                        final NObject acc = rawThreadAccessorLocalThread.get();
                        final float rt = (float) (double) acc.invoke("RetentionTimeFromScanNumber", scanIndex);
                        if ((rt < minRtToProcess) || (rt > maxRtToProcess)) return null;
                        final NObject spectrum = centroid ? acc.invoke("GetSimplifiedCentroids", scanIndex) : rawThreadAccessorLocalThread.get().invoke("GetSimplifiedScan", scanIndex);
                        //if (LOG.isDebugEnabled()) {
                        final NObject filter = acc.invoke("GetFilterForScanNumber", scanIndex);
                        final double precursorMz = filter.invoke("GetMass", 0);
                        if ((prevPrecursorMz[0] != -Double.MAX_VALUE) && (precursorMz != prevPrecursorMz[0]))
                            throw new RuntimeException("Not same precursor mz");
                        prevPrecursorMz[0] = precursorMz;
                        // }
                        final double[] mz = spectrum.get("Masses");
                        final double[] intensity = spectrum.get("Intensities");
                        final Float2FloatAVLTreeMap mzAucIntensities = new Float2FloatAVLTreeMap();
                        final int mzSize = mz.length;
                        //initial accumulating vars declaration
                        final DoubleArrayList tempLstMz = new DoubleArrayList(5);
                        final FloatArrayList temLstI = new FloatArrayList(5);
                        float totIntensity = 0f;
                        float localMaxI = -Float.MAX_VALUE;
                        float localMinI = Float.MAX_VALUE;
                        for (int i = 0; i < mzSize; i++) {

                            final double curMz = mz[i];
                            final float curI = (float) intensity[i];
                            localMaxI = Math.max(curI, localMaxI);
                            localMinI = Math.min(curI, localMinI);
                            final double nextMz = ((i + 1) < mzSize) ? mz[i + 1] : Double.MAX_VALUE;

                            //TODO get rid of this merging logic....I'd rather not have it (esp for thermo data...)
                            //  numDpBeforeMerges.incrementAndGet();
                            //    WiffReader.determineIfWeShouldMerge(diaWindowToProcess, baseline, minMzToProcess, maxMzToProcess, rt, mzAucIntensities, tempLstMz, temLstI, curMz, curI, nextMz);
                            mzAucIntensities.put((float) curMz, curI);
                            totIntensity += curI;
//
//                            if ((mz[i] >= minMzToProcess) && (mz[i] <= maxMzToProcess) && ((float) intensity[i] >= baseline)) {
//                                mzAucIntensities.put(mz[i], (float) intensity[i]);
//                            }
                        }

                        final float dynamicRange = localMaxI / localMinI;
                        maxI.accumulate(localMaxI);
                        minI.accumulate(localMinI);
                        maxDynamicRange.accumulate(dynamicRange);
                        if (mzSize > 0) {
                            minMs2.accumulate(mz[0]);
                            maxMs2.accumulate(mz[mzSize - 1]);
                        }

                        lstNumPeaksPerWindow.get(diaWindowToProcess).addAndGet(mzSize);
                        maxNumPeaksInSpectrum.accumulate(mzSize);
                        lstTotIntensityPerRtIntv.get(DoubleMath.roundToInt(rt / ImportMsFile.rtIntervalInMinutes, RoundingMode.HALF_UP)).addAndGet(totIntensity);
                        lstNumPeaksPerRtIntv.get(DoubleMath.roundToInt(rt / ImportMsFile.rtIntervalInMinutes, RoundingMode.HALF_UP)).addAndGet(mzSize);

                        final ImmutableDeconvolutedMzSpectrum imm = ImmutableDeconvolutedMzSpectrum
                                .builder()
                                .setDIAWindowNum(diaWindowToProcess)
                                .setRtInMinutes(rt)
                                .setMzAUCIntensities(mzAucIntensities)
                                .build();
                        //    System.out.println("Imm is {}", imm);
                        return imm;
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                })
                .filter(immutableDeconvolutedMzSpectrum -> immutableDeconvolutedMzSpectrum != null);
    }

    @Override
    public MsHeaderInfo getMsHeaderInfo() {
        return thermoHeaderInfo;
    }
}
