//package com.deepdia.realtimeuploader.extractmsdata;
//
//import com.google.common.collect.ComparisonChain;
//import it.unimi.dsi.fastutil.doubles.Double2FloatAVLTreeMap;
//import org.immutables.value.Value;
//
//import java.io.Serializable;
//
//
///**
// * @author gautam
// */
//@Value.Immutable
//@Value.Style(init = "set*", stagedBuilder = true, strictBuilder = true)
//public abstract class DeconvolutedMzSpectrum implements Comparable<DeconvolutedMzSpectrum>, Serializable {
//    private static final long serialVersionUID = 1L;
//
//    // vars
//    public abstract int getDIAWindowNum();
//
//    public abstract float getRtInMinutes();
//
//    public abstract Double2FloatAVLTreeMap getMzAUCIntensities();
//
//    public ImmutableDeconvolutedMzSpectrum clone() {
//        return ImmutableDeconvolutedMzSpectrum
//                .builder()
//                .setDIAWindowNum(getDIAWindowNum())
//                .setRtInMinutes(getRtInMinutes())
//                .setMzAUCIntensities(getMzAUCIntensities().clone())
//                .build();
//    }
//
//    // ************************************************
//    // COMPARATOR
//    // ************************************************
//    @Override
//    public int compareTo(DeconvolutedMzSpectrum o) {
//        return ComparisonChain.start()
//                .compare(getDIAWindowNum(), o.getDIAWindowNum())
//                .compare(getRtInMinutes(), o.getRtInMinutes())
//                .result();
//    }
//
//}
//
