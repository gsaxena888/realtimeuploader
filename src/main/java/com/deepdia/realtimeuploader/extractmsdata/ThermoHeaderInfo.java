package com.deepdia.realtimeuploader.extractmsdata;


import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.immutables.value.Value;
import systems.deepsearch.common.pojo.ms.embedded.ImmutableDIAWindowHeader;


import java.io.Serializable;
import java.util.HashMap;

@Value.Immutable
@Value.Style(init = "set*", stagedBuilder = true, strictBuilder = true)
public abstract class ThermoHeaderInfo implements MsHeaderInfo, Serializable {
    private static final long serialVersionUID = 1L;

    @Override
    public abstract String getFileName();

//   @Override  @Value.Auxiliary
//    public abstract long getFileSize() ;

    @Override
    @Value.Auxiliary
    public abstract String getMsManufacturer();

    @Override
    @Value.Auxiliary
    public abstract String getMsModel();

//    @Override @Value.Auxiliary
//    public abstract float getFirsRtMinutesOverAllWindows();
//
//    @Override @Value.Auxiliary
//    public abstract float getLastRtMinutesOverAllWindows();
//
//   @Override  @Value.Auxiliary
//    public abstract double getMinMzForMs2();
//
//    @Override @Value.Auxiliary
//    public abstract double getMaxMzForMs2();

//    @Override @Value.Auxiliary
//    public abstract int getNumScans();

    @Override
    @Value.Auxiliary
    public abstract HashMap<Integer, ImmutableDIAWindowHeader> getAllDIAWindowHeadersByDiaWindowNum(); //TODO in retrospect, this map could actually be an (immutable) list, but it's probabl not worth the effort to change....

    @Override
    public ImmutableDIAWindowHeader getOneDiaWindowHeaderByDiaWindowNum(int diaWindowNum) {
        return getAllDIAWindowHeadersByDiaWindowNum().get(diaWindowNum);
    }

    @Override
    @Value.Auxiliary
    public abstract int getNumDIAWindows();

    //TOSTRING (might be slow technique, since it uses reflection; so only use if speed won't be that critical.
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}
