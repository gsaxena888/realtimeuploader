package com.deepdia.realtimeuploader.extractmsdata;


import com.google.common.util.concurrent.AtomicDouble;
import systems.deepsearch.common.pojo.ms.ImmutableDeconvolutedMzSpectrum;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.DoubleAccumulator;
import java.util.concurrent.atomic.LongAccumulator;
import java.util.stream.Stream;

public interface InputFileExtractor {

    public Stream<ImmutableRawDiaSpectrum> getRawSpectrumStream(final int minDiaWindowToProcess, final int maxDiaWindowToProcess, final float minRtToProcess, final float maxRtToProcess, final double minMzToProcess, final double maxMzToProcess, final float baseline);

    public Stream<ImmutableDeconvolutedMzSpectrum> getDeconvolutedSpectrumStream(float minRtToProcess, float maxRtToProcess, double minMzToProcess, double maxMzToProcess, final float baseline, final int expNum, List<AtomicInteger> lstNumPeaksPerWindow, List<AtomicDouble> lstTotIntensityPerRtIntv, DoubleAccumulator maxDynamicRange, DoubleAccumulator maxI, DoubleAccumulator minI, List<AtomicDouble> lstNumPeaksPerRtIntv, LongAccumulator maxNumPeaksInSpectrum, AtomicInteger totalSpectra, DoubleAccumulator minMs1, DoubleAccumulator maxMs1, DoubleAccumulator minMs2, DoubleAccumulator maxMs2);

    public MsHeaderInfo getMsHeaderInfo();
}
