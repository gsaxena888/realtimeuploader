package com.deepdia.realtimeuploader.extractmsdata.mzxmlextractor.entities;

import org.immutables.value.Value;


/**
 * @author gautam
 */
@Value.Immutable(builder = false)
@Value.Style(init = "set*")
public abstract class MinMzMaxMz {

    // vars
    @Value.Parameter
    public abstract double getMinMz();

    @Value.Parameter
    public abstract double getMaxMz();

}

