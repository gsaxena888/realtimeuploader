//package com.deepdia.deepsearch.modules.inputfileextractors.mzxmlextractor;
//
//import com.deepdia.realtimeuploader.common.misc.Utilities;
//import ImmutableDeconvolutedMzSpectrum;
//import ImmutableRawDiaSpectrum;
//import InputFileExtractor;
//import ImmutableMzXmlHeaderInfo;
//import ImmutableMzXmlPartition;
//import MzXmlHeaderInfo;
//import MzXmlPartition;
//import IntStreamUnordered;
//import MapPartitionNumToMzXmlPartition;
//import MapRawStringSpectrumToRawSpectrum;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import java.util.Collections;
//import java.util.List;
//import java.util.Objects;
//import java.util.stream.Collectors;
//import java.util.stream.Stream;
//
//public class DoMzXmlExtraction implements InputFileExtractor  {
//    private static final Logger LOG = LoggerFactory.getLogger(DoMzXmlExtraction.class);
//    private final List<ImmutableMzXmlPartition> lstOfMzXMLPartitions;
//
//    public ImmutableMzXmlHeaderInfo getMsHeaderInfo() {
//        return mzXmlHeaderInfo;
//    }
//
//    private final ImmutableMzXmlHeaderInfo mzXmlHeaderInfo;
//
//    public DoMzXmlExtraction(String inputFile) {
//        final ImmutableMzXmlHeaderInfo mzXmlHeaderInfo = MzXmlHeaderInfo.of(inputFile); //this will immediately start reading the first 1000 or so spectra from the mzXML file (to calculate things like cycle time etc) and also read the header info (to getAnyOrder things like the expected number of scans etc.)
//        this.mzXmlHeaderInfo = mzXmlHeaderInfo;
//
//
//        final int loadFactor = 4; //TODO try values like 2 or 3 and see what happens
//        final int totPartitions = Utilities.getNumThreads() * loadFactor; // the number of parititions we intend to break up the mzXml file into
//
//        //*************************************************************
//        // Determine how the giant mzXML could be split into multiple partitions (this involves some very "light" reading of the mzXML file)
//        //*************************************************************
//        List<ImmutableMzXmlPartition> lstOfMzXMLPartitions =
//                IntStreamUnordered
//                        .range(0, totPartitions - 1)
//                        .parallelStream() //this is the key area where we paralellize everything; everything else later will be "single threaded" within this parallel stream
//                        .map(new MapPartitionNumToMzXmlPartition(mzXmlHeaderInfo.getFileName(), mzXmlHeaderInfo.getFileSize(), totPartitions))
//                        .collect(Collectors.collectingAndThen(Collectors.toList(), lst -> {
//                            Collections.sort(lst);
//                            return lst;
//                        }));
//
//        final int sizeMinus1 = lstOfMzXMLPartitions.size() - 1;
//        for (int i = 0; i < sizeMinus1; i++) {
//            final ImmutableMzXmlPartition curPartition = lstOfMzXMLPartitions.get(i);
//            final ImmutableMzXmlPartition newPartition = curPartition.withOvershotScanNumString(lstOfMzXMLPartitions.get(i + 1).getStartingScanNumString());
//            lstOfMzXMLPartitions.set(i, newPartition);
//        }
//        // set last partition (special way)
//        final ImmutableMzXmlPartition newLastPartition = lstOfMzXMLPartitions.get(sizeMinus1).withOvershotScanNumString(mzXmlHeaderInfo.getLastScanNumString());
//        lstOfMzXMLPartitions.set(sizeMinus1, newLastPartition);
//        lstOfMzXMLPartitions = Collections.unmodifiableList(lstOfMzXMLPartitions);
//
//        LOG.info("lstOfMzXMLParitions is " + lstOfMzXMLPartitions);
//        this.lstOfMzXMLPartitions = lstOfMzXMLPartitions;
//    }
//
//    @Override
//    public Stream<ImmutableRawDiaSpectrum> getRawSpectrumStream(final int minDiaWindowToProcess, final int maxDiaWindowToProcess, final float minRtToProcess, final float maxRtToProcess, final double minMzToProcess, final double maxMzToProcess, final float baseline) {
//
//        return lstOfMzXMLPartitions
//                .parallelStream()
//                //    .stream()
//                .flatMap(MzXmlPartition::stream)
//                .filter(Objects::nonNull) // this is needed since the "last" element returned from  mzXmlPartition.stream() will be null
//                //   .filter(rawStringSpectrum -> rawStringSpectrum.getMsLevel().equals("2")) // we only care about DIA MS2 windows' spectra
//                //TODO getDGivenIdx rid of this filter of only ms2 data; instead, let's bring it all in and then later filter, as needed.
//                .filter(rawStringSpectrum -> rawStringSpectrum.getMsLevel().equals("2"))
//                .filter(rawStringSpectrum -> {
//                    final int diaWindowNum = mzXmlHeaderInfo.toDIAWindowNum(Double.parseDouble(rawStringSpectrum.getPrecursorMz())); //precursorMz will never be null at this point, since we're only dealing with MS2 at this stage....
//                    return (minDiaWindowToProcess <= diaWindowNum) && (diaWindowNum <= maxDiaWindowToProcess);
//                })
//                .filter(rawStringSpectrum -> Utilities.convertRtStringToFloat(rawStringSpectrum.getRetentionTime()) > minRtToProcess && Utilities.convertRtStringToFloat(rawStringSpectrum.getRetentionTime()) <= maxRtToProcess) // we only care about DIA MS2 windows' spectra
//                //.filter(new FilterSubsetofRawSpectrum(allowAllDataToPass, dso, mzXmlHeaderInfo)) //we only filter for DIAWindow num and RT in this stage; to filter for mz values, that can only be (efficiently) done within the MapRawStringSpectrumToRawSpectrum transformation below
//                .map(new MapRawStringSpectrumToRawSpectrum(minMzToProcess, maxMzToProcess, mzXmlHeaderInfo, baseline));
//    }
//
//    @Override
//    public Stream<ImmutableDeconvolutedMzSpectrum> getDeconvolutedSpectrumStream(float minRtToProcess, float maxRtToProcess, double minMzToProcess, double maxMzToProcess, float baseline, final int expNum) {
//        throw new RuntimeException("This is not supported here; the logic for this is elsewhere");
//    }
//}
