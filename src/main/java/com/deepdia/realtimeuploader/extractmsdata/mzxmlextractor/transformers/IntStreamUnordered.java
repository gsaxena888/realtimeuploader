package com.deepdia.realtimeuploader.extractmsdata.mzxmlextractor.transformers;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * Created by labsite-guest on 8/29/2017.
 */
public class IntStreamUnordered implements Spliterator<Integer> {
    private static final Logger LOG = LoggerFactory.getLogger(IntStreamUnordered.class);

    private int startInclusive; //low index, inclusive
    private int endExclusive; //high index, EXclusive ; or in other woders, if startInclusive =0, then this value is the numEdges of the implied array range....

    // PRIVE constructor
    private IntStreamUnordered(final int startInclusive, final int endExclusive) {
        this.startInclusive = startInclusive;
        //   this.curIdx = this.startInclusive;
        this.endExclusive = endExclusive;
    }

    //THIS IS THE MAIN METHOD THROUGH WHICH DEVELOPERS CAN CALL THIS CLASS
    public static IntStreamUnordered range(int startInclusive, int endExclusive) {
        return new IntStreamUnordered(startInclusive, endExclusive);
    }

    // AND THEN, AFTER CALLING ABOVE METHOD, THEY CAN CALL STREAM OR PARALLELSTREAM. THis way, the interface is similar to IntStream
    public Stream<Integer> stream() {
        return StreamSupport.stream(this, false);
    }

    public Stream<Integer> parallelStream() {
        return StreamSupport.stream(this, true);
    }


    // ***************************
    // THE BELOW SECTION IS NOT JUST THE SPLITERATOR METHODS
    // ***************************

    @Override
    public boolean tryAdvance(final Consumer<? super Integer> action) {
        final int curIdx = startInclusive;
        if (curIdx < endExclusive) {
            startInclusive++;
            action.accept(curIdx);
            return true;
        } else return false;
    }

    @Override
    public Spliterator<Integer> trySplit() {
        LOG.warn("Doing split wiht start {} and end {}  on thread {}", startInclusive, endExclusive, Thread.currentThread().getId());
        // the logic here is similar (but much simpler) than the logic in the jdk ArrayList's ArrayListSplitetaror methods
        final int mid = (startInclusive + endExclusive) / 2; // rounds/truncates down
        if (startInclusive >= mid) {
            return null; //nothing leftLst to split
        }

        final int oldLoIdx = startInclusive;
        startInclusive = mid; //reset the low index so now this object is the upper half of the split
        //LOG.trace("After split in current object: currently, Low " + startInclusive + " mid " + mid + " high " + endExclusive);
        return new IntStreamUnordered(oldLoIdx, mid); // and this new object that's returned is the lower half of split
    }

    @Override
    public long estimateSize() {
        return endExclusive - startInclusive;
    }

    @Override
    public int characteristics() {
        //   return Spliterator.ORDERED | Spliterator.SIZED | Spliterator.SUBSIZED |              Spliterator.IMMUTABLE | Spliterator.NONNULL |             Spliterator.DISTINCT;// | Spliterator.SORTED;
        return Spliterator.NONNULL | Spliterator.IMMUTABLE | Spliterator.DISTINCT | Spliterator.SIZED | Spliterator.SUBSIZED;
        //    return Spliterator.SIZED | Spliterator.SUBSIZED;
        //   return  Spliterator.DISTINCT | Spliterator.SIZED | Spliterator.SUBSIZED;
        // return  Spliterator.SIZED | Spliterator.SUBSIZED;
    }
}
