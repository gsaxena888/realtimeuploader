package com.deepdia.realtimeuploader.extractmsdata.mzxmlextractor.transformers;

import com.deepdia.realtimeuploader.extractmsdata.mzxmlextractor.entities.ImmutableMzXmlPartition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

/**
 * Created by labsite-guest on 8/30/2017.
 */
public class MapPartitionNumToMzXmlPartition implements java.util.function.Function<Integer, ImmutableMzXmlPartition> {
    private static final Logger LOG = LoggerFactory.getLogger(MapPartitionNumToMzXmlPartition.class);

    private final String fileName;
    private final long bytesPerPartition;


    public MapPartitionNumToMzXmlPartition(String fileName, long fileSize, int totPartitions) {
        this.fileName = fileName;
        this.bytesPerPartition = fileSize / totPartitions;
    }

    @Override
    public ImmutableMzXmlPartition apply(Integer partitionNum) {
        final long approxStartBytes = bytesPerPartition * partitionNum;
        final Charset charset = Charset.forName("US-ASCII");
        try (FileChannel fc = FileChannel.open(Paths.get(fileName), StandardOpenOption.READ)) {
            fc.position(approxStartBytes);
            ByteBuffer buffer = ByteBuffer.allocate(32 * 120000); //TODO verify if this is too large or not large enough for worse case scenario; if it's taking too long, we can iterate over small buffers and check one char at a time (in sequence) for the tag "<scan num"
            //  ByteBuffer buffer = ByteBuffer.allocate(32 * 6); //TODO verify if this is too large or not large enough for worse case scenario; if it's taking too long, we can iterate over small buffers and check one char at a time (in sequence) for the tag "<scan num"
            if (fc.read(buffer) >= 0) {
                fc.close();  //we can rightLst away close the fc; we no longer need it

                final String bufString = charset
                        .decode((ByteBuffer) buffer.flip())
                        .toString();
                final String SCANTAG = "<scan num=\"";
                final long idx = bufString.indexOf(SCANTAG);
                if (idx < 0) throw new RuntimeException("Couldn't find \"<scan num\" in buffer");
                final int startIdxOfScanNum = (int) idx + SCANTAG.length();
                String scanNumString = bufString.substring(startIdxOfScanNum, startIdxOfScanNum + 20);
                scanNumString = scanNumString.substring(0, scanNumString.indexOf("\""));

                final long exactStartBytes = approxStartBytes + idx;
//                FileChannel position = fc.position(exactStartBytes);
//                final InputStream is = Channels.newInputStream(fc);

//                final StringBuilder out = new StringBuilder();
//                final char[] tempBuffer = new char[10];
//                Reader in = new InputStreamReader(is, "US-ASCII");
//                int rsz = in.read(tempBuffer, 0, tempBuffer.length);
//                out.append(tempBuffer, 0, rsz);
//                LOG.info("Is reader says first 10 chars are " + out.toString());

                final ImmutableMzXmlPartition mzXMLPartition = ImmutableMzXmlPartition.builder()
                        .setFileName(fileName)
                        .setPartitionNum(partitionNum)
                        .setStartBytes(exactStartBytes)
                        .setStartingScanNumString(scanNumString)
                        .setOvershotScanNumString("UNKNOWN AT THIS TIME")
                        .build();
                //LOG.debug("The mzXML Parttion is {} ", mzXMLPartition);
                return mzXMLPartition;
            } else throw new RuntimeException("buffer was somehow empty");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
