package com.deepdia.realtimeuploader.extractmsdata.mzxmlextractor.transformers;

import com.deepdia.realtimeuploader.common.misc.Utilities;
import com.deepdia.realtimeuploader.extractmsdata.ImmutableRawDiaSpectrum;
import com.deepdia.realtimeuploader.extractmsdata.ModifiableStartIdx;
import com.deepdia.realtimeuploader.extractmsdata.mzxmlextractor.entities.ImmutableMzXmlHeaderInfo;
import com.deepdia.realtimeuploader.extractmsdata.mzxmlextractor.entities.ImmutableRawStringSpectrum;
import com.deepdia.realtimeuploader.extractmsdata.mzxmlextractor.entities.NumRegionsHackWrapper;
import com.deepdia.realtimeuploader.extractmsdata.mzxmlextractor.entities.RawStringSpectrum;
import it.unimi.dsi.fastutil.doubles.DoubleArrayList;
import it.unimi.dsi.fastutil.floats.FloatArrayList;
import javolution.text.CharArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import umich.ms.datatypes.scan.PeaksCompression;
import umich.ms.fileio.exceptions.FileParsingException;
import umich.ms.fileio.filetypes.mzxml.MZXMLPeaksDecoder;
import umich.ms.util.ByteArrayHolder;
import umich.ms.util.base64.Base64Context;
import umich.ms.util.base64.Base64ContextPooled;

import java.io.IOException;
import java.util.zip.DataFormatException;

/**
 * Created by labsite-guest on 8/16/2017.
 */
public class MapRawStringSpectrumToRawSpectrum implements java.util.function.Function<ImmutableRawStringSpectrum, ImmutableRawDiaSpectrum> {
    private static final Logger LOG = LoggerFactory.getLogger(MapRawStringSpectrumToRawSpectrum.class);
    // final Utilities dso;
    private final double minMzToProcess;
    private final double maxMzToProcess;
    private final ImmutableMzXmlHeaderInfo mzXmlHeaderInfo;
    private final float baseline;

    public MapRawStringSpectrumToRawSpectrum(final double minMzToProcess, final double maxMzToProcess, ImmutableMzXmlHeaderInfo mzXmlHeaderInfo, float baseline) {
        this.minMzToProcess = minMzToProcess;
        this.maxMzToProcess = maxMzToProcess;
        this.mzXmlHeaderInfo = mzXmlHeaderInfo;
        this.baseline = baseline;
    }

    public static MZXMLPeaksDecoder.DecodedData extractMzIntFromRawStringSpectrum(RawStringSpectrum rawStringSpectrum) {
        // This code is borrowed from the MS File ToolBox from BatMass.
        // Extract mz-intensity pairs
        final MZXMLPeaksDecoder.DecodedData decoded;
        try {
            //LOG.debug("Working on rawstrignspectrum scan num: {} ", rawStringSpectrum.getScanNum());
            umich.ms.util.base64.Base64 base64 = new umich.ms.util.base64.Base64();
            Base64Context ctx = new Base64ContextPooled(); // this ctx will borrow a ByteArrayHolder from pool
            CharArray chars = new CharArray(rawStringSpectrum.getMzInt());
            //LOG.trace("MzInt is: {}" , chars);
            Base64Context decodedB64 = base64.decode(chars.array(), chars.offset(), chars.length(), ctx);
            ByteArrayHolder bah = decodedB64.readResults();

            PeaksCompression compression = PeaksCompression.NONE;
            //   if (rawStringSpectrum.getCompressionType() != null) { //I don't think null is ever possible, but I could be wrong -Gautam
            if ("zlib".equalsIgnoreCase(rawStringSpectrum.getCompressionType())) {
                compression = PeaksCompression.ZLIB;
            }
            //   }

            decoded = MZXMLPeaksDecoder.decode(
                    bah.getUnderlyingBytes(), bah.getPosition(),
                    Integer.parseInt(rawStringSpectrum.getPrecision()), compression);
            ctx.close();

        } catch (FileParsingException e) {
            throw new RuntimeException(e);
        } catch (DataFormatException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return decoded;
    }

    @Override
    public ImmutableRawDiaSpectrum apply(ImmutableRawStringSpectrum rawStringSpectrum) {
        final float rt;
        rt = Utilities.convertRtStringToFloat(rawStringSpectrum.getRetentionTime());
        final MZXMLPeaksDecoder.DecodedData decoded = extractMzIntFromRawStringSpectrum(rawStringSpectrum);

        final DoubleArrayList mzsAsList;
        final FloatArrayList intensitiesAsList;
//        if ((minMzToProcess == 0) && (maxMzToProcess == Float.MAX_VALUE)) {
//            mzsAsList = DoubleArrayList.wrap(decoded.mzs);
//            intensitiesAsList = DoubleArrayList.wrap(decoded.intensities);
//        }
//        else {
        final double[] mzs = decoded.mzs;
        final double[] intensities = decoded.intensities;
        final int size = mzs.length;
        mzsAsList = new DoubleArrayList();
        intensitiesAsList = new FloatArrayList();
        for (int i = 0; i < size; i++) {
            final double curMz = mzs[i];
            final double curI = intensities[i];
            if ((minMzToProcess <= curMz) && (curMz <= maxMzToProcess) && (curI > baseline)) {
                mzsAsList.add(curMz);
                intensitiesAsList.add((float) intensities[i]);
                //            intensitiesAsList.add(intensities[i] / 1000d);
            }
        }
        mzsAsList.trim();
        intensitiesAsList.trim();
//        }

        return ImmutableRawDiaSpectrum
                .builder()
                .setRtInMinutes(rt)
                .setDiaWindowNum(mzXmlHeaderInfo.toDIAWindowNum(Double.parseDouble(rawStringSpectrum.getPrecursorMz())))
                // .setScanNum(Integer.parseInt(rawStringSpectrum.getScanNum()))
                //  .setPrecursorMz(Double.parseDouble(rawStringSpectrum.getPrecursorMz()))
                //.setPrecursorMz(-Double.MAX_VALUE) //we don't know what it is
                //    .setPrecursorMzTolerance(Double.parseDouble(rawStringSpectrum.getWindowWideness()) / 2d)
                //  .setPrecursorMzTolerance(-Double.MAX_VALUE) //we don't know what it is
                //  .setPrecursorCharge(Integer.MIN_VALUE)
                .setMzs(mzsAsList)
                .setIntensities(intensitiesAsList)
                .setNumRegions(new NumRegionsHackWrapper())
                .setModifiableStartIdx(new ModifiableStartIdx())
                .build();

    }

}
