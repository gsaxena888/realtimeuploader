package com.deepdia.realtimeuploader.extractmsdata.mzxmlextractor.entities;

import com.deepdia.realtimeuploader.common.misc.Utilities;
import org.codehaus.stax2.XMLInputFactory2;
import org.codehaus.stax2.XMLStreamReader2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.SequenceInputStream;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Iterator;

/**
 * Created by labsite-guest on 8/20/2017.
 */
public class MzXmlIterator implements Iterator<ImmutableRawStringSpectrum> {
    private static final Logger LOG = LoggerFactory.getLogger(MzXmlIterator.class);

    private final InputStream inputStream;
    private final ImmutableMzXmlHeaderInfo.Builder parentMzXmlBuilder;
    private final String overshotScanNumString;
    private final XMLStreamReader2 reader;
    private final FileChannel fc;
    private final org.apache.commons.lang.mutable.MutableBoolean isOrbi;

    private boolean overshot = false;


    public MzXmlIterator(String fileName, long startBytes, String overshotScanNumString) {
        this(fileName, startBytes, null, overshotScanNumString, new org.apache.commons.lang.mutable.MutableBoolean());
    }

    public MzXmlIterator(String fileName, ImmutableMzXmlHeaderInfo.Builder parentMzXmlBuilder, String overshotScanNumString, org.apache.commons.lang.mutable.MutableBoolean isOrbi) {
        this(fileName, 0, parentMzXmlBuilder, overshotScanNumString, isOrbi);
    }

    private MzXmlIterator(String fileName, long startBytes, ImmutableMzXmlHeaderInfo.Builder parentMzXmlBuilder, String overshotScanNumString, org.apache.commons.lang.mutable.MutableBoolean isOrbi) {
        try {
            this.fc = FileChannel.open(Paths.get(fileName), StandardOpenOption.READ);
            fc.position(startBytes); //TODO verify that we don't need to captur output....

            if (parentMzXmlBuilder == null) {
                final String msRunString = "<msRun>";
                InputStream msRunHeaderStream = new ByteArrayInputStream(msRunString.getBytes(StandardCharsets.UTF_8));
                this.inputStream = new SequenceInputStream(msRunHeaderStream, Channels.newInputStream(fc));
            } else this.inputStream = Channels.newInputStream(fc);
            this.parentMzXmlBuilder = parentMzXmlBuilder;
            this.overshotScanNumString = overshotScanNumString;
            this.isOrbi = isOrbi;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        final XMLInputFactory2 factory = (XMLInputFactory2) XMLInputFactory2.newInstance();
//       factory.configureForLowMemUsage();
        factory.configureForSpeed();
        factory.setProperty(XMLInputFactory.IS_COALESCING, true); //critical; without this, mzInt data in xml file gets truncated without warning!
        try {
            reader = (XMLStreamReader2) factory.createXMLStreamReader(inputStream);
        } catch (XMLStreamException e) {
            throw new RuntimeException(e);
        }
    }


    @Override
    public boolean hasNext() {
        return !overshot;
    } //TODO this is a little hacky; see next TODO rightLst after next() call

    @Override
    public ImmutableRawStringSpectrum next() {
        try {
            //initialize some vars
            final ImmutableRawStringSpectrum.Builder rawSpectrumBuilder = ImmutableRawStringSpectrum.builder();
            String tagContent = null;
            boolean alreadySetRawFile = false;
            final XMLStreamReader2 locReader = reader; //this is just a performance trick to bring "field" into local var.

            while (locReader.hasNext()) {
                final int event = locReader.next();
                switch (event) {
                    case XMLStreamConstants.START_ELEMENT:
                        if ("scan".equals(locReader.getLocalName())) {
                            final String scanNum = locReader.getAttributeValue(null, "num");
                            //LOG.trace("Scan num is {} and overShootScanNum is {}" , scanNum, overshotScanNumString);
                            if (scanNum.equals(overshotScanNumString)) { //*********** THis is the one of the two ways we detemine if we've overshot; the other way is in the "end element" section of msRun
                                //LOG.trace("Doing end routine");
                                doEndRoutine(locReader);
                                return null; // NOTE: We return null, so we do need some logic to filter out nulls unfortunately...
                            }
                            rawSpectrumBuilder.setScanNum(scanNum);
                            //TODO find out if getbyPosition is (way) faster?
                            rawSpectrumBuilder.setRetentionTime(locReader.getAttributeValue(null, "retentionTime"));
                            rawSpectrumBuilder.setMsLevel(locReader.getAttributeValue(null, "msLevel"));
                        } else if ("peaks".equals(locReader.getLocalName())) {
                            rawSpectrumBuilder.setCompressionType(locReader.getAttributeValue(null, "compressionType"));
                            rawSpectrumBuilder.setPrecision(locReader.getAttributeValue(null, "precision"));
                        } else if ((parentMzXmlBuilder != null) && ("msRun".equals(locReader.getLocalName()))) {
                            parentMzXmlBuilder.setNumScans(Integer.parseInt(locReader.getAttributeValue(null, "scanCount")));
                            parentMzXmlBuilder.setFirsRtMinutesOverAllWindows(Utilities.convertRtStringToFloat(locReader.getAttributeValue(null, "startTime")));
                            parentMzXmlBuilder.setLastRtMinutesOverAllWindows(Utilities.convertRtStringToFloat(locReader.getAttributeValue(null, "endTime")));
                        } else if ((parentMzXmlBuilder != null) && ("parentFile".equals(locReader.getLocalName()))) {
                            if (!alreadySetRawFile) {
                                parentMzXmlBuilder.setRawFileSHA(locReader.getAttributeValue(null, "fileSha1"));
                                alreadySetRawFile = true;
                            }
                        } else if ((parentMzXmlBuilder != null) && ("msManufacturer".equals(locReader.getLocalName()))) {
                            parentMzXmlBuilder.setMsManufacturer(locReader.getAttributeValue(null, "value"));
                        } else if ((parentMzXmlBuilder != null) && ("msModel".equals(locReader.getLocalName()))) {
                            final String msModelStrig = locReader.getAttributeValue(null, "value");
                            if (msModelStrig.contains("Orbitrap")) isOrbi.setValue(true);
                            else isOrbi.setValue(false);
                            parentMzXmlBuilder.setMsModel(msModelStrig);
                        } else if ((parentMzXmlBuilder != null) && ("precursorMz".equals(locReader.getLocalName()))) {
                            rawSpectrumBuilder.setWindowWideness(locReader.getAttributeValue(null, "windowWideness"));
                        }
                        break;

                    case XMLStreamConstants.CHARACTERS:
                        tagContent = locReader.getText(); //I don't think we need to trim  since these mzXML files are assumed to be properly (and automatically) generated. -Gautam
                        break;

                    case XMLStreamConstants.END_ELEMENT:
                        switch (locReader.getLocalName()) {
                            case "msRun": // *********** This is the one of the two ways we detemine if we've overshot; the other way is in the "start element" section of scanNum
                                doEndRoutine(locReader);
                                return null;
                            case "scan":
                                ImmutableRawStringSpectrum rawSpectrum = rawSpectrumBuilder.build();
                                //LOG.trace("Built rawSpectrum with scanNum {}" , rawSpectrum.getScanNum());
                                return rawSpectrum;
                            case "precursorMz":
                                //LOG.trace("Precursor mz is {}" , tagContent);
                                rawSpectrumBuilder.setPrecursorMz(tagContent);
                                break;
                            case "peaks":
                                rawSpectrumBuilder.setMzInt(tagContent);
                                break;
                        }
                        break;

                    case XMLStreamConstants.START_DOCUMENT:
                        break;
                }
            }
        } catch (XMLStreamException e) {
            try {
                reader.close();
            } catch (XMLStreamException e1) {
                LOG.error("We couldn't even close the locReader after the original exception was thrown!!! Here's the closing error exception " + e1);
            }
            throw new RuntimeException(e);
        }
        throw new RuntimeException("Somehow, the mzXML iterator got to a point it should never have been able to getAnyOrder to! This could be because the mzXML file has no scans!");
    }

    private void doEndRoutine(XMLStreamReader2 locReader) {
        overshot = true;
        try {
            locReader.close();
            inputStream.close();
            fc.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (XMLStreamException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("mzXML files are read-only; no element can be removed");
    }
}
