package com.deepdia.realtimeuploader.extractmsdata.mzxmlextractor.entities;


import com.deepdia.realtimeuploader.common.misc.Utilities;
import com.google.common.collect.ComparisonChain;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.immutables.value.Value;

import javax.annotation.Nullable;

@Value.Immutable
@Value.Style(init = "set*")
public abstract class RawStringSpectrum implements Comparable<RawStringSpectrum> {
    public static ImmutableRawStringSpectrum getPoisonSpectrum() {
        return ImmutableRawStringSpectrum.builder()
                .setScanNum("POISON")
                .setMsLevel("")
                .setRetentionTime("")
                .setCompressionType("")
                .setPrecision("")
                .setMzInt("")
                .setPrecursorMz("")
                .setWindowWideness("")
                .build();
    }

    // vars
    // the meanings of these vars are best described in the definitions of the mzXML xml specification (and possibly ProteoWizards msconvert program too)
    public abstract String getScanNum();

    @Value.Auxiliary
    public abstract String getMsLevel();

    @Value.Auxiliary
    public abstract String getRetentionTime();

    @Value.Auxiliary
    public abstract String getCompressionType();

    @Value.Auxiliary
    public abstract String getPrecision();

    @Value.Auxiliary
    public abstract String getMzInt();

    @Nullable //nullable, since MS1 data does not have a precursorMz
    @Value.Auxiliary
    public abstract String getPrecursorMz();

    @Nullable
    //nullable, since MS1 data does not have a precursorMz and also because we don't need to capture this info when building rawSpectrum for iteration (it's only needed for header info)
    @Value.Auxiliary
    public abstract String getWindowWideness();

    //tostring
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int compareTo(RawStringSpectrum o) {
        return ComparisonChain.start()
                .compare(Utilities.convertRtStringToFloat(this.getRetentionTime()), Utilities.convertRtStringToFloat(o.getRetentionTime()))
                .result();
    }

    ;

}
