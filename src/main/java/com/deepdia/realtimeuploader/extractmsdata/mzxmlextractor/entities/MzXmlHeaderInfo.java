package com.deepdia.realtimeuploader.extractmsdata.mzxmlextractor.entities;

import com.deepdia.realtimeuploader.common.misc.Utilities;
import com.deepdia.realtimeuploader.extractmsdata.MsHeaderInfo;
import com.deepdia.realtimeuploader.extractmsdata.mzxmlextractor.transformers.MapRawStringSpectrumToRawSpectrum;
import com.google.common.collect.ImmutableMap;
import gnu.trove.list.array.TFloatArrayList;
import org.apache.commons.lang.mutable.MutableBoolean;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.immutables.value.Value;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import systems.deepsearch.common.pojo.ms.embedded.ImmutableDIAWindowHeader;
import umich.ms.fileio.filetypes.mzxml.MZXMLPeaksDecoder;

import java.io.*;
import java.util.*;

import static java.util.stream.Collectors.*;

@Value.Immutable
@Value.Style(init = "set*", strictBuilder = false, builderVisibility = Value.Style.BuilderVisibility.PACKAGE)
//WARNING!!!!!!!!!!!!! In this "special" class, the builderVisibility has been set to PRIVATE (see this line). Therefore, please use the custom .of() method that you'll see at the end of this pacakge to create this class
public abstract class MzXmlHeaderInfo implements Serializable, MsHeaderInfo {
    private static final long serialVersionUID = 1L;

    private static final Logger LOG = LoggerFactory.getLogger(MzXmlHeaderInfo.class);

    // ***************************************
    //constructor
    // *********************************************
    public static ImmutableMzXmlHeaderInfo of(final String fileName) {
        try {
            return getMzXmlHeaderInfoAndFirst1000Scans(fileName, new FileInputStream(new File(fileName)));
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    private static ImmutableMzXmlHeaderInfo getMzXmlHeaderInfoAndFirst1000Scans(final String fileName, final InputStream inputStream) {
        // final int numScansToPreview = 1000; //TODO change this to 200*10 (200 is max DIA windows I've seen; and I'm just guessing that 10 is a decent number of cycles....but, need to make sure this will still work if you hit end of file fist... -Gautam
        // final int assumedFirstScanNum = 1;
        final String overshotScanNumString = "1000"; //TODO THIS IS A POTENTIAL BUG!!!!!! If that scan number doesn't exist, this solution won't work....one hack is that we can convert string to int and then do a >= comparison in iterator....

        ImmutableMzXmlHeaderInfo.Builder builder = ImmutableMzXmlHeaderInfo.builder()
                .setFileName(fileName)
                .setFileSize((new File(fileName)).length());

        final MzXmlIterator itr;
        final org.apache.commons.lang.mutable.MutableBoolean isOrbi = new MutableBoolean(); //a hacky way of getting info back from mzXmlIterator
        try {
            itr = new MzXmlIterator(fileName, builder, overshotScanNumString, isOrbi); //since we pass of, the iterator can modify it as needed to fill in the needed fields
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        //  Map<Float, ImmutableList<RawStringSpectrum>>
        final Map<ImmutablePrecursorMz, List<ImmutableRawStringSpectrum>> precursorMz_FirstFewRawStringSpectra = Utilities.createDistinctImmutableStreamFromIterator(itr)
                .parallel()
                .filter(Objects::nonNull)
                //   .peek(rawStringSpectrum -> LOG.info("RawStringSpectrum is " + rawStringSpectrum) )
                .collect(groupingBy
                        (
                                rawStringSpectrum -> ImmutablePrecursorMz.of(Double.parseDouble((rawStringSpectrum.getPrecursorMz() == null) ? "0" : rawStringSpectrum.getPrecursorMz())),
                                collectingAndThen(toList(), lst -> {
                                    Collections.sort(lst);
                                    return Collections.unmodifiableList(lst);
                                })
                        )
                ); //for fun/learning, can change to groupbyconcurrent and see if results change?

        builder.setFirstScanNum(
                precursorMz_FirstFewRawStringSpectra
                        .entrySet()
                        .stream()
                        .mapToInt(firstFewScanAWindow -> Integer.parseInt(firstFewScanAWindow.getValue().get(0).getScanNum()))
                        .min()
                        .getAsInt()
        );

        final List<ImmutableMinMzMaxMz> minMzMaxMzsForMs2Only = precursorMz_FirstFewRawStringSpectra
                .entrySet()
                .stream()
                .flatMap(precursorMz_lstRawStringSpectrum -> precursorMz_lstRawStringSpectrum.getValue().stream())
                .filter(rawStringSpectrum -> rawStringSpectrum.getMsLevel().equals("2")) // we only care about DIA MS2 windows' spectra
                .map(rawStringSpectrum -> {
                    MZXMLPeaksDecoder.DecodedData mzInts = MapRawStringSpectrumToRawSpectrum.extractMzIntFromRawStringSpectrum(rawStringSpectrum);
                    double[] mzs = mzInts.mzs;
                    return ImmutableMinMzMaxMz.of(mzs[0], mzs[mzs.length - 1]);
                })
                .collect(toList());

        double minMzForMs2 = -10 + //buffer room
                minMzMaxMzsForMs2Only
                        .stream()
                        .mapToDouble(MinMzMaxMz::getMinMz)
                        .min()
                        .getAsDouble();
        if (minMzForMs2 <= 0) {
            LOG.warn("The minimum mz was derived to be " + minMzForMs2 + " which is <= zero; so we reset it to 0.1 .... This may cause issues");
            minMzForMs2 = 0.1d;
        }
        builder.setMinMzForMs2(minMzForMs2);

        builder.setMaxMzForMs2(+150 + //buffer room
                minMzMaxMzsForMs2Only
                        .stream()
                        .mapToDouble(MinMzMaxMz::getMaxMz)
                        .max()
                        .getAsDouble()
        );

        // TODO need to complete logic....
        final double approxResAtMinMz = calcResolution(isOrbi.booleanValue(), minMzForMs2);
        final double approxFwhmAtMinMz = minMzForMs2 / approxResAtMinMz;
        LOG.info("The res at minMz of " + minMzForMs2 + " is " + approxResAtMinMz + " and so the approxFwhm at that min mz is " + approxFwhmAtMinMz);
        builder.setApproxFwhmAtMinMz(approxFwhmAtMinMz);

        final float cycleTimeInMin = (float) (double) precursorMz_FirstFewRawStringSpectra.entrySet()
                .stream() //for fun/learning, can change to parallelstream and see if results change?
                .filter(firstFewScansForAWindow -> firstFewScansForAWindow.getKey().getPrecursorMz() != 0) //filter out those entries that have precursorMZ of 0, ie the MS1 entries
                .map(e -> {
                    List<ImmutableRawStringSpectrum> firstFewRawScansPerDIAWindow = e.getValue();//.stream().filter(firstFewScansPerDIAWindow -> firstFewScansPerDIAWindow.getMsLevel().equals("2")).collect(toList());
                    final TFloatArrayList cycleTimesList = new TFloatArrayList(firstFewRawScansPerDIAWindow.size() - 1);
                    for (int i = 0; i < firstFewRawScansPerDIAWindow.size() - 1; i++) {
                        cycleTimesList.add(Utilities.convertRtStringToFloat(firstFewRawScansPerDIAWindow.get(i + 1).getRetentionTime()) - Utilities.convertRtStringToFloat(firstFewRawScansPerDIAWindow.get(i).getRetentionTime()));
                    }
                    cycleTimesList.sort();
                    //LOG.debug("CycleTime list {} " , cycleTimesList.toString());
                    return cycleTimesList;
                })
                .collect(averagingDouble(cycleTimesList -> cycleTimesList.get(cycleTimesList.size() / 2))); //mapper returns the median, and then we just average it across the different exp windows
        builder.setCycleTimeMin(cycleTimeInMin);

        final int[] diaWindowNumber = {0};
        HashMap<Integer, ImmutableDIAWindowHeader> gsQuickFixHMap = new HashMap<>();
        precursorMz_FirstFewRawStringSpectra.entrySet()
                .stream() //can this be a parallelStream, since we have a trimAndSort later? (This is just for my learning; it won't change performance in any meaningful way) -Gautam
                .filter(firstFewScansForAWindow -> firstFewScansForAWindow.getKey().getPrecursorMz() != 0) //filter out those entries that have precursorMZ of 0, ie the MS1 entries
                .sorted(Comparator.comparingDouble(kv -> kv.getKey().getPrecursorMz())) //this should trimAndSort the the precursorMz_FirstFewRawStringSpectra key values, which corresponds to the precursorMz of each DIA window
                .forEach(kv -> {
                    final RawStringSpectrum firstScan = kv.getValue().get(0); //first scan for **this** DIA DiaWindow
                    final double precursorMzDouble = Double.parseDouble(firstScan.getPrecursorMz());
                    final ImmutableDIAWindowHeader myDIAWindowHeader =
//                            ImmutableDIAWindowHeader.builder()
//                            .setDIAWindowNumber(diaWindowNumber[0])
//                            .setDIAWindowMzCenter(precursorMzDouble)
//                            .setDIAWindowMzWidth(Double.parseDouble(firstScan.getWindowWideness()))
//                            .setFirstRtMin(Utilities.convertRtStringToFloat(firstScan.getRetentionTime()))
//                            .build();
                            new ImmutableDIAWindowHeader(diaWindowNumber[0], precursorMzDouble, Double.parseDouble(firstScan.getWindowWideness()), Utilities.convertRtStringToFloat(firstScan.getRetentionTime()));
                    builder.putAllDIAWindowHeadersByPrecursorMz(precursorMzDouble, myDIAWindowHeader);
                    gsQuickFixHMap.put(diaWindowNumber[0], myDIAWindowHeader);
                    diaWindowNumber[0]++;
                });
        builder.setAllDIAWindowHeadersByDiaWindowNum(gsQuickFixHMap);
        ImmutableMzXmlHeaderInfo mzXML = builder.build();
        LOG.info("mzXML file definition: " + mzXML.toString());
        return mzXML;
    }

    private static double calcResolution(boolean isOrbi, double minMzForMs2) {
        //TODO this should be a calculation/derivation using the fist few scans (just like we're doing above to determine minMzForMs2, MaxMz etc). Need Dennis's help. For now, for QTOF I'm setting it to ~30k and for Orbi, ~78k (at approx ~90Th). I'm guessing that it's slightly safer to safer to err on the side of an assumed higer res than the true res...
        if (isOrbi) {
            //we'll assume that resoution is 35k at mz of 200Th. Per TODO above, this should really be derived from the data....
            final double resAt200Th = 35000d;
            return resAt200Th / Math.sqrt(minMzForMs2 / 200d);
        } else { //TRIPLE TOF is what is assumed based on my analysis of SGS gold standard data in water medium (per Ruedi's OpenSwath Nature paper);
            return 14000d; //30000d; //9000d; //7000d;
        }
    }

    // *************************
    // Getters for vars
    // ***************************
    public abstract String getFileName();

    @Value.Auxiliary
    public abstract long getFileSize();

    @Value.Auxiliary
    public abstract String rawFileSHA();

    @Value.Auxiliary
    public abstract String getMsManufacturer();

    @Value.Auxiliary
    public abstract String getMsModel();

    @Value.Auxiliary
    public abstract float getFirsRtMinutesOverAllWindows();

    @Value.Auxiliary
    public abstract float getLastRtMinutesOverAllWindows();

    @Value.Auxiliary
    public abstract double getMinMzForMs2();

    @Value.Auxiliary
    public abstract double getMaxMzForMs2();

    @Value.Auxiliary
    public abstract double getApproxFwhmAtMinMz();

    @Value.Auxiliary
    public abstract int getNumScans();

    @Value.Auxiliary
    public abstract int getFirstScanNum();

    //TODO we should verify what happens to scan numbers if the msconvert command has some trimAndSort of filter (eg skip RT50to55) that causes data in middle to be filtered out; then, are the scan numbers still continuous or do the scan numbers actually refer exactly to the scan number in th eMS files.....depeding on answer, it will cause a bug in out logics
    @Value.Auxiliary
    @Value.Derived
    public String getLastScanNumString() {
        return String.valueOf(getFirstScanNum() + getNumScans() - 1);
    }

    @Value.Auxiliary
    public abstract float getCycleTimeMin();

    @Value.Auxiliary
    public abstract ImmutableMap<Double, ImmutableDIAWindowHeader> getAllDIAWindowHeadersByPrecursorMz();

    @Value.Auxiliary
    public abstract HashMap<Integer, ImmutableDIAWindowHeader> getAllDIAWindowHeadersByDiaWindowNum();

    public ImmutableDIAWindowHeader getOneDiaWindowHeaderByDiaWindowNum(int diaWindowNum) {
        return getAllDIAWindowHeadersByDiaWindowNum().get(diaWindowNum);
    }

    //  @Value.Derived
    public int getNumDIAWindows() {
        return getAllDIAWindowHeadersByPrecursorMz().size();
    }

    //   @Value.Derived
    public int getUpperBoundSpectraPerWindow() {
        return 1 + (int) ((getLastRtMinutesOverAllWindows() - getFirsRtMinutesOverAllWindows()) / getCycleTimeMin());
    }

    public int toDIAWindowNum(double precursorMz) {
        ImmutableMap<Double, ImmutableDIAWindowHeader> diaWindowHeaders = getAllDIAWindowHeadersByPrecursorMz();
        //LOG.trace("Precursor mz is {}" , precursorMz);
        ImmutableDIAWindowHeader diaWindowHeader = diaWindowHeaders.get(precursorMz);
        return diaWindowHeader.getDiaWindowNumber();
    }

    // *********************************
    // Other methods
    // *********************************

    //TOSTRING (might be slow technique, since it uses reflection; so only use if speed won't be that critical.
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }

}

