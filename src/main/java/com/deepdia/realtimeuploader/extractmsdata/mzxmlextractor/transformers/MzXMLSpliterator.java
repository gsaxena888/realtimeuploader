//package com.deepdia.deepsearch.modules.inputFileExtractors.mzxmlextractor.transformers;
//
//import ImmutableRawStringSpectrum;
//import RawStringSpectrum;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import java.util.Spliterator;
//import java.util.concurrent.BlockingQueue;
//import java.util.concurrent.TimeUnit;
//import java.util.function.Consumer;
//
//
///**
// * Created by labsite-guest on 8/28/2017.
// */
//public class MzXMLSpliterator implements Spliterator<ImmutableRawStringSpectrum> {
//
//    private static final Logger LOG = LoggerFactory.getLogger(MzXMLSpliterator.class);
//
//    public MzXMLSpliterator() { }
//
//
//    @Override
//    public boolean tryAdvance(Consumer<? super ImmutableRawStringSpectrum> action) {
//
//        return true;
//    }
//
//
//    @Override
//    public Spliterator<ImmutableRawStringSpectrum> trySplit() {
//        return null;
//    }
//
//
//    @Override
//    public long estimateSize() {
//        return Long.MAX_VALUE;
//    }
//
//    @Override
//    public int characteristics() {
//        return Spliterator.DISTINCT | Spliterator.SIZED | Spliterator.NONNULL | Spliterator.IMMUTABLE;
//    }
//}
