package com.deepdia.realtimeuploader.extractmsdata.mzxmlextractor.entities;


import com.deepdia.realtimeuploader.common.misc.Utilities;
import com.google.common.collect.ComparisonChain;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.immutables.value.Value;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.stream.Stream;


@Value.Immutable
@Value.Style(init = "set*", strictBuilder = true, stagedBuilder = true)
public abstract class MzXmlPartition implements Comparable<ImmutableMzXmlPartition> {

    private static final Logger LOG = LoggerFactory.getLogger(MzXmlPartition.class);

    // *************************
    // vars
    // ***************************
    public abstract String getFileName();

    public abstract int getPartitionNum();

    @Value.Auxiliary
    public abstract long getStartBytes();

    @Value.Auxiliary
    public abstract String getStartingScanNumString();

    @Value.Auxiliary
    public abstract String getOvershotScanNumString();

    //TOSTRING (might be slow technique, since it uses reflection; so only use if speed won't be that critical.
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int compareTo(ImmutableMzXmlPartition o) {
        return ComparisonChain.start()
                .compare(this.getPartitionNum(), o.getPartitionNum())
                .result();
    }

    // *********************************
    // Other methods
    // *********************************
    public Stream<ImmutableRawStringSpectrum> stream() {
        return Utilities.createDistinctImmutableStreamFromIterator(new MzXmlIterator(getFileName(), getStartBytes(), getOvershotScanNumString()));
    }

}

