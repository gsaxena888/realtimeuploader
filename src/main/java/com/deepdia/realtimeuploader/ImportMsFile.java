package com.deepdia.realtimeuploader;

import com.deepdia.realtimeuploader.extractmsdata.InputFileExtractor;
import com.deepdia.realtimeuploader.extractmsdata.MsHeaderInfo;
import com.deepdia.realtimeuploader.extractmsdata.sciexextractor.DoSciexExtraction;
import com.deepdia.realtimeuploader.extractmsdata.thermoextractor.DoThermoExtraction;
import com.deepdia.realtimeuploader.pojo.ConfigSettings;
import com.google.common.math.DoubleMath;
import com.google.common.util.concurrent.AtomicDouble;
import it.unimi.dsi.fastutil.floats.FloatArrayList;
import it.unimi.dsi.fastutil.ints.IntArrayList;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import net.lingala.zip4j.io.outputstream.ZipOutputStream;
import net.lingala.zip4j.model.ZipParameters;
import org.apache.commons.io.FileUtils;
import systems.deepsearch.common.pojo.general.embedded.LogicalFileDefn;
import systems.deepsearch.common.pojo.ms.ImmutableDeconvolutedMzSpectrum;
import systems.deepsearch.common.pojo.ms.Ms2Chromatagrams;
import systems.deepsearch.common.pojo.ms.MsFile;
import systems.deepsearch.common.pojo.ms.embedded.ImmutableDIAWindowHeader;
import systems.deepsearch.common.util.MADOutlierDetetector;
import systems.deepsearch.common.util.ProgressCounter;
import systems.deepsearch.common.util.RestApiPaths;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import java.io.*;
import java.math.RoundingMode;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.DoubleAccumulator;
import java.util.concurrent.atomic.LongAccumulator;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.zip.ZipEntry;

import static com.deepdia.realtimeuploader.MyUtils.serialize;
import static com.deepdia.realtimeuploader.MyUtils.serializeStream;

@Slf4j
public class ImportMsFile {

    public final static float rtIntervalInMinutes = 0.20f;
    public static final String OUTPUT_FOLDER = "C:\\Users\\mboutaskiouine\\";
    //public static final String TEMP_FOLDER = "./temp/";
    private final File fileToImport;
    //   private final WebTarget apiUrl;
    private MsFile msFileFromRti; //this is not marked finaly, but it really shouldn't change....

    public ImportMsFile(final File fileToImport) {
        this.fileToImport = fileToImport;
        // apiUrl = generateApiUrl(ConfigSettings.getInstance());
        startImport();
    }

    @SneakyThrows
    private void startImport() {
        //determine if file already exists in system (ie previously imported)
//        if (hasFileAlreadyBeenImported(fileToImport)) {
//            log.warn("The file {} has already been imported. It will not be re-imported. See https://xxx.xx.xxx for additional helpful info", fileToImport);
//        }
//        else {
        log.info("Import starting for {}", fileToImport);
        final InputFileExtractor inputFileExtractor = getInputFileExtractor(fileToImport);

        //final List<String> orderedUploadUrls =
        getOrderedUploadUrls(fileToImport, inputFileExtractor);
//            if (orderedUploadUrls.get(0).equals(Constants.FILE_ALREADY_IMPORTED) ) {
//                log.warn("This file {} has already been imported. It will not be reimported!", fileToImport);
//            }
//            else {

        final List<AtomicInteger> lstNumPeaksPerWindow = new ArrayList<>(inputFileExtractor.getMsHeaderInfo().getNumDIAWindows());
        IntStream.range(0, inputFileExtractor.getMsHeaderInfo().getNumDIAWindows()).forEach(idx -> lstNumPeaksPerWindow.add(new AtomicInteger(0)));

        final int initialSize = DoubleMath.roundToInt(5f * 60f / rtIntervalInMinutes, RoundingMode.HALF_UP);
        final List<AtomicDouble> lstTotIntensityPerRtIntv = new ArrayList<>(initialSize);
        IntStream.range(0, initialSize).forEach(idx -> lstTotIntensityPerRtIntv.add(new AtomicDouble(0)));
        final List<AtomicDouble> lstNumPeaksPerRtIntv = new ArrayList<>(initialSize);
        IntStream.range(0, initialSize).forEach(idx -> lstNumPeaksPerRtIntv.add(new AtomicDouble(0)));


        final DoubleAccumulator maxI = new DoubleAccumulator(Double::max, -Double.MAX_VALUE);
        final DoubleAccumulator minI = new DoubleAccumulator(Double::min, Double.MAX_VALUE);
        final DoubleAccumulator maxDynamicRange = new DoubleAccumulator(Double::max, -Double.MAX_VALUE);
        final DoubleAccumulator maxRt = new DoubleAccumulator(Double::max, -Double.MAX_VALUE);

        final DoubleAccumulator minMs1 = new DoubleAccumulator(Double::min, Double.MAX_VALUE);
        final DoubleAccumulator maxMs1 = new DoubleAccumulator(Double::max, -Double.MAX_VALUE);
        final DoubleAccumulator minMs2 = new DoubleAccumulator(Double::min, Double.MAX_VALUE);
        final DoubleAccumulator maxMs2 = new DoubleAccumulator(Double::max, -Double.MAX_VALUE);

        final LongAccumulator maxNumPeaksInSpectrum = new LongAccumulator(Long::max, -Long.MAX_VALUE);
        //final AtomicLong totalPeaks = new AtomicLong(); //can be trivially derived from lstNumPeaksPerWindow
        final AtomicInteger totalSpectra = new AtomicInteger();

        final FloatArrayList lstRtCyclesInMin = new FloatArrayList(msFileFromRti.getNumDiaWindows() * 2000);
        final int numDiaWindows = inputFileExtractor.getMsHeaderInfo().getNumDIAWindows();
        final ProgressCounter pc = ProgressCounter.of("Processing: " + fileToImport.getName(), numDiaWindows, 0.10f, 10, true);
        pc.start();

        // ByteArrayOutputStream x = serializeStream(msFileFromRti, new ByteArrayOutputStream());
        ZipParameters zipParameters = new ZipParameters();


        final String fileNameOfZippedFile = OUTPUT_FOLDER + fileToImport.getName() + ".ds";
      //  final String fileNameOfZippedFile = "./test.zip";
        try(ZipOutputStream zos = initializeZipOutputStream(new File(fileNameOfZippedFile))) {
            //for (File fileToAdd : filesToAdd) {





            IntStream.range(0, inputFileExtractor.getMsHeaderInfo().getNumDIAWindows())
                    .sequential() //TODO make parallel?
                    .forEachOrdered(diaWindowNum -> {
                        final ArrayList<ImmutableDeconvolutedMzSpectrum> centroidedSpectrumForOneWindow = vendorCentroid(inputFileExtractor, diaWindowNum, lstNumPeaksPerWindow, lstTotIntensityPerRtIntv, maxDynamicRange, maxI, minI, lstNumPeaksPerRtIntv, maxNumPeaksInSpectrum, totalSpectra, minMs1, maxMs1, minMs2, maxMs2);
                        maxRt.accumulate(centroidedSpectrumForOneWindow.get(centroidedSpectrumForOneWindow.size() - 1).getRtInMinutes());

                        //now, let's accumulate the cycle times....
                        for (int i = 1, end = centroidedSpectrumForOneWindow.size(); i < end; i++) {
                            final float rtCycleInMin = (centroidedSpectrumForOneWindow.get(i).getRtInMinutes() - centroidedSpectrumForOneWindow.get(i - 1).getRtInMinutes());
                            lstRtCyclesInMin.add(rtCycleInMin);
                        }

                        final String fileNameOfDs = fileToImport.getName() + "~" + diaWindowNum + ".ds";
                        //centroidedSpectrumForOneWindow
                        //byte[] byteArray = Util.fstConf.asByteArray(centroidedSpectrumForOneWindow);

                        zipParameters.setFileNameInZip(fileNameOfDs);
                        try {
                            zos.putNextEntry(zipParameters);
                            zos.write(serialize(centroidedSpectrumForOneWindow));
                            zos.closeEntry();
                        } catch (Exception e) {
                            throw new RuntimeException(e);
                        }
//                        try {
//                            FileUtils.writeByteArrayToFile(new File(fileNameOfDs), byteArray);
//                        } catch (IOException e) {
//                            throw new RuntimeException(e);
//                        }

                        pc.step();
                    });
            pc.finish();
            //now, let's calculate the cycle time stats
            final MADOutlierDetetector md = MADOutlierDetetector.of(lstRtCyclesInMin);
            msFileFromRti.setMinCycleTimeInSec(md.getMin() * 60f);
            msFileFromRti.setMaxCycleTimeInSec(md.getMax() * 60f);
            msFileFromRti.setMedianCycleTimeInSec(md.getMedian() * 60f);
            msFileFromRti.setMeanCycleTimeInSec(md.getMean() * 60f);
            msFileFromRti.setCvCycleTime(md.getCVUsingMean());

            //now, let's update msfile info with latest stats
            msFileFromRti.setMs2Chromatagrams(createMs2Chromatagrams(inputFileExtractor, lstNumPeaksPerWindow, lstTotIntensityPerRtIntv, lstNumPeaksPerRtIntv, maxI, minI, maxDynamicRange, maxRt, maxNumPeaksInSpectrum, totalSpectra));
            msFileFromRti.setMinMs2Mz(minMs2.floatValue()); //TODO eventually, this should not be derived from the data itself, but we should be able to directly read this infomratin from somewhere in the raw file
            msFileFromRti.setMaxMs2Mz(maxMs2.floatValue()); //TODO eventually, this should not be derived from the data itself, but we should be able to directly read this infomratin from somewhere in the raw file

            msFileFromRti.setStatus(MsFile.MsFileStatus.READY);
            final String fileNameOfDs = fileToImport.getName() + "~" + "msFile" + ".ds";
            //centroidedSpectrumForOneWindow
            //byte[] byteArray = Util.fstConf.asByteArray(msFileFromRti);
            zipParameters.setFileNameInZip(fileNameOfDs);
            zos.putNextEntry(zipParameters);
         //   serializeStream(msFileFromRti, zos);
            zos.write(serialize(msFileFromRti));
            zos.closeEntry();
//            try (InputStream inputStream = new FileInputStream(fileToAdd)) {
//                while ((readLen = inputStream.read(buff)) != -1) {
//                    zos.write(buff, 0, readLen);
//                }
//            }
            //byte[] byteArray = serialize(msFileFromRti);

//            try {
//                FileUtils.writeByteArrayToFile(new File(fileNameOfDs), byteArray);
//            } catch (IOException e) {
//                throw new RuntimeException(e);
//            }



        }
    }

    private ZipOutputStream initializeZipOutputStream(File outputZipFile)
            throws IOException {
        FileOutputStream fos = new FileOutputStream(outputZipFile);
        return new ZipOutputStream(fos);
    }

    private Ms2Chromatagrams createMs2Chromatagrams(InputFileExtractor inputFileExtractor, List<AtomicInteger> lstNumPeaksPerWindow, List<AtomicDouble> lstTotIntensityPerRtIntv, List<AtomicDouble> lstNumPeaksPerRtIntv, DoubleAccumulator maxI, DoubleAccumulator minI, DoubleAccumulator maxDynamicRange, DoubleAccumulator maxRt, LongAccumulator maxNumPeaksInSpectrum, AtomicInteger totalSpectra) {
        final long totalPeaks = lstNumPeaksPerWindow.stream().mapToLong(numPeaks -> numPeaks.get()).sum();
        final int trueSizeOfRtArray = DoubleMath.roundToInt(maxRt.get() / rtIntervalInMinutes, RoundingMode.UP);
        log.info("True Size of RtArray is {} vs original size of {}", trueSizeOfRtArray, lstNumPeaksPerRtIntv.size());
        final FloatArrayList lstTotalIntensityForAllPeaksPerRtInterval = new FloatArrayList(trueSizeOfRtArray);
        lstTotIntensityPerRtIntv.stream().limit(trueSizeOfRtArray).forEachOrdered(totIntPeaks -> lstTotalIntensityForAllPeaksPerRtInterval.add((float) totIntPeaks.get()));

        final IntArrayList lstTotalNumPeaksPerRtInterval = new IntArrayList(trueSizeOfRtArray);
        lstNumPeaksPerRtIntv.stream().limit(trueSizeOfRtArray).forEachOrdered(numPeaks -> lstTotalNumPeaksPerRtInterval.add(numPeaks.intValue()));
        log.info("lstTotalNumPeaksPerRtInterval size before trimming is {}", lstTotalNumPeaksPerRtInterval.size());
        lstTotalNumPeaksPerRtInterval.trim();
        log.info("lstTotalNumPeaksPerRtInterval size after trimming is {}", lstTotalNumPeaksPerRtInterval.size());

        final float totalIntensity = (float) lstTotalIntensityForAllPeaksPerRtInterval.stream().mapToDouble(totIntensityPerRt -> totIntensityPerRt).sum();
        final IntArrayList lstMsPeaksPerWindow = new IntArrayList(inputFileExtractor.getMsHeaderInfo().getNumDIAWindows());
        lstNumPeaksPerWindow.stream().forEachOrdered(numPeaksInOneWindow -> lstMsPeaksPerWindow.add(numPeaksInOneWindow.get()));

        return new Ms2Chromatagrams(rtIntervalInMinutes, lstTotalNumPeaksPerRtInterval, lstTotalIntensityForAllPeaksPerRtInterval, lstMsPeaksPerWindow, (float) maxDynamicRange.get(), (float) maxI.get(), (float) minI.get(), (float) maxRt.get(), totalSpectra.get(), totalPeaks, totalIntensity, (int) maxNumPeaksInSpectrum.get());
    }

    private boolean hasFileAlreadyBeenImported(File fileToImport) {
        //TODO
        return false;
    }

    private void getOrderedUploadUrls(final File fileToImport, final InputFileExtractor inputFileExtractor) {
       // final ConfigSettings config = ConfigSettings.getInstance();


        //TODO to complete
        final MsHeaderInfo headerInfo = inputFileExtractor.getMsHeaderInfo();
        //inputFileExtractor.getMsHeaderInfo().
        final HashMap<Integer, ImmutableDIAWindowHeader> diaWindowHeaders = headerInfo.getAllDIAWindowHeadersByDiaWindowNum();
        final float agc = -Float.MAX_VALUE; //TODO get from actual file....
        final float minMs1 = (float) diaWindowHeaders.get(0).deriveDIAWindowMzStart();
        final float maxMs1 = (float) diaWindowHeaders.get(headerInfo.getNumDIAWindows() - 1).deriveDIAWindowMzEnd();

        msFileFromRti = (new MsFile(
                new LogicalFileDefn(fileToImport.getName())
                        .setMsLastUpdateDateTime(new Date(fileToImport.lastModified())) //TODO this ends up being the modified date of hte file, which is often not the actual time the file was created (because users copy/paste files); instead, there should be a way of getting this info from the actual raw file itself....
                        .setRtiMachineInfo(null)
                        .setSize(fileToImport.length()),
                (short) headerInfo.getNumDIAWindows(), diaWindowHeaders, MsFile.MsFileStatus.IMPORTING, agc, minMs1, maxMs1))
                .setFullPath(Paths.get(fileToImport.getAbsolutePath()).getParent().toString())
                .setMsManufacturer(headerInfo.getMsManufacturer())
                .setMsModel(headerInfo.getMsModel())
                .setStartImportDateTime(new Date())
                .setRtiBldVer((short) 0);

   //     if (config.isDoChecksum()) {
     //       final String xxHash64 = "todo"; //TODO
      //      msFileFromRti.setXxHash64(xxHash64);
       // }

//        final List<String> signedUrls = apiUrl
//                .path(RestApiPaths.IMPORT_MS_FILE)
//                .request(MediaType.APPLICATION_JSON)
//                .post(Entity.entity(msFileFromRti, MediaType.APPLICATION_JSON))
//                .readEntity(new GenericType<List<String>>() {});
        //  log.debug("signedUrls is {}", signedUrls);
        //    return signedUrls;
    }

    private WebTarget generateApiUrl(ConfigSettings config) {
        final String APP_URL = config.getAppUrl();
        final String BASE_URI = APP_URL + RestApiPaths.BASE_API_PATH;
        final Client client = ClientBuilder.newClient();
        return client.target(BASE_URI);
    }

    private InputFileExtractor getInputFileExtractor(File fileToImport) {
        final InputFileExtractor inputFileExtractor;
        if (fileToImport.getName().toLowerCase().endsWith(".wiff")) {
            inputFileExtractor = new DoSciexExtraction(fileToImport);
        } else if (fileToImport.getName().toLowerCase().endsWith(".raw")) {
            inputFileExtractor = new DoThermoExtraction(fileToImport);
        } else {
            throw new RuntimeException("We do not yet support this file type. Your input file was " + fileToImport.getAbsolutePath() + ". The only supported file type are .raw and .wiff; please contact DeepDIA support if you would like this other file type supported.");
        }
        return inputFileExtractor;
    }

    private ArrayList<ImmutableDeconvolutedMzSpectrum> vendorCentroid(final InputFileExtractor inputFileExtractor, final int diaWindowToPRocess, List<AtomicInteger> lstNumPeaksPerWindow, List<AtomicDouble> lstTotIntensityPerRtIntv, DoubleAccumulator maxDynamicRange, DoubleAccumulator maxI, DoubleAccumulator minI, List<AtomicDouble> lstNumPeaksPerRtIntv, LongAccumulator maxNumPeaksInSpectrum, AtomicInteger totalSpectra, DoubleAccumulator minMs1, DoubleAccumulator maxMs1, DoubleAccumulator minMs2, DoubleAccumulator maxMs2) {

        //  log.info("About to read the whole file");
        final ArrayList<ImmutableDeconvolutedMzSpectrum> deconvolutedMzSortedSpectraForOneDiaWindow =
                (ArrayList<ImmutableDeconvolutedMzSpectrum>) inputFileExtractor.getDeconvolutedSpectrumStream(0, Integer.MAX_VALUE, 0, Float.MAX_VALUE, 0, diaWindowToPRocess, lstNumPeaksPerWindow, lstTotIntensityPerRtIntv, maxDynamicRange, maxI, minI, lstNumPeaksPerRtIntv, maxNumPeaksInSpectrum, totalSpectra, minMs1, maxMs1, minMs2, maxMs2)
                        .collect(Collectors.toList()); //TODO if this is taking a while, we should change this to grouping to a list (indexed by diaWindowNum); and we can also trimAndSort data before returnin results (and it can done in parallel)
        // DoThermoExtraction dt = (DoThermoExtraction) inputFileExtractor;
        //log.info("dt.numDpBeforeMerges is {} and after is {} and merges is {}", dt.numDpBeforeMerges, dt.numDpAfterMerges, dt.numMerges);
        log.debug("Done with deconvolutedMzSortedSpectraForOneDiaWindow with numEdges of {}.", deconvolutedMzSortedSpectraForOneDiaWindow.size());
        Collections.sort(deconvolutedMzSortedSpectraForOneDiaWindow);
        log.debug("Done with SORTING deconvolutedMzSortedSpectraForOneDiaWindow' lists");

        return deconvolutedMzSortedSpectraForOneDiaWindow;
    }
}
