package com.deepdia.realtimeuploader.pojo;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.experimental.FieldNameConstants;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.URL;
import systems.deepsearch.common.pojo.general.RtiMachineInfo;
import systems.deepsearch.common.pojo.general.RtiMachineInfoDto;
import systems.deepsearch.common.util.Constants;
import systems.deepsearch.common.util.RestApiPaths;
import systems.deepsearch.common.util.Util;

import javax.validation.constraints.NotBlank;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Slf4j
@Data
@Accessors(chain = true)
@FieldNameConstants
//@EqualsAndHashCode(onlyExplicitlyIncluded = true) //all fields participate in equals and hashcode....
public class ConfigSettings {
    private static volatile ConfigSettings mInstance;

    @NonNull @NotBlank @URL private String appUrl;
    private boolean doChecksum;
    private int rtiBldVer;
    private RtiMachineInfoDto rtiMachineInfo;

    private ConfigSettings() {
        //TODO replace below code and instead read info from ConfigSettings.yaml file (which needs to be created of course).
     //   appUrl = "http://127.0.0.1:8080";
        appUrl = "https://test.cgsupercomputer.com";
        doChecksum = false;
        rtiBldVer = 1;
        rtiMachineInfo = retrieveDummyMachineInfo(appUrl);
    }

    private RtiMachineInfoDto retrieveDummyMachineInfo(final String APP_URL) {
        final String BASE_URI = APP_URL + RestApiPaths.BASE_API_PATH;
        final Client client = ClientBuilder.newClient();
        final WebTarget apiUrl = client.target(BASE_URI);
        final RtiMachineInfoDto rtiMachineInfo = apiUrl
                .path(RestApiPaths.GET_DUMMY_RTI_MACHINE)
                .request(MediaType.APPLICATION_JSON)
                .get(RtiMachineInfoDto.class);
        log.debug("RtiMachineInfo is {}", rtiMachineInfo);
        return rtiMachineInfo;
    }

    public static ConfigSettings getInstance() {
        if (mInstance == null) {
            synchronized (ConfigSettings.class) {
                if (mInstance == null) {
                    mInstance = new ConfigSettings();
                }
            }
        }
        return mInstance;
    }

}
