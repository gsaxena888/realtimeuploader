package com.deepdia.realtimeuploader.common.misc;

public class Constants {

    public static final double MAX_FRACTION_OF_FWHM_FOR_FEATURE_LINKING = 0.5d; //TODO don't hardcode, and analyze ideal value.
}
