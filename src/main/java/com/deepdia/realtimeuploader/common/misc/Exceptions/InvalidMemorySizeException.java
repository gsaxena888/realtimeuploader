package com.deepdia.realtimeuploader.common.misc.Exceptions;

public class InvalidMemorySizeException extends RuntimeException {

    private String size;

    public InvalidMemorySizeException(String size) {
        this.size = size;
    }

    @Override
    public String getMessage() {
        return "Invalid memory getNumEdges, the minimum requirement is " + size;
    }
}
