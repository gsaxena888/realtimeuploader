package com.deepdia.realtimeuploader.common.misc;




import com.deepdia.realtimeuploader.ImportMsFile;
import com.google.common.base.Preconditions;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.math3.util.FastMath;
import org.nustaq.serialization.FSTConfiguration;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Date;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@Slf4j
public class Utilities {

    // public static final  OperatingSystemMXBean osBean = ManagementFactory.getPlatformMXBean(OperatingSystemMXBean.class);

    public final static boolean SAFE_MODE = false;
    public static final FSTConfiguration fstConf = FSTConfiguration.createDefaultConfiguration();
    static float expadjust[];
    static double minExp;
    static double maxExp;
    static float minExpValue;
    static float maxExpValue;

    // ************************
    // STATIC UTILITY METHODS
    // ************************
    //Utilities to convert an interator into a streaming solution.
    public static <T> Stream<T> createStreamFromIterator(Iterator<T> it) {

        return createStreamFromIteratorSlave(() -> it);
    }

    public static <T> Stream<T> createDistinctImmutableStreamFromIterator(Iterator<T> it) {
        return createDistinctImmutableStreamFromIteratorSlave(() -> it);
    }


    // ***************************
    // NON-static UTILITY METHODS
    // *****************************

    private static <T> Stream<T> createStreamFromIteratorSlave(Iterable<T> it) {
        return StreamSupport.stream(it.spliterator(), false);
    }

    private static <T> Stream<T> createDistinctImmutableStreamFromIteratorSlave(Iterable<T> it) {
        Spliterator<T> spliterator = Spliterators.spliteratorUnknownSize(it.iterator(), Spliterator.DISTINCT | Spliterator.IMMUTABLE | Spliterator.SIZED);
        return StreamSupport.stream(spliterator, false);
    }

    public static <T> Stream<T> createParallelImmutableStreamFromIterator(Iterable<T> it) {
        Spliterator<T> spliterator = Spliterators.spliteratorUnknownSize(it.iterator(), Spliterator.DISTINCT | Spliterator.IMMUTABLE | Spliterator.SIZED | Spliterator.SUBSIZED | Spliterator.NONNULL);
        return StreamSupport.stream(spliterator, true);
    }

    public static float convertRtStringToFloat(String rtTimeString) {
        try {
            return (float) (DatatypeFactory.newInstance().newDuration(rtTimeString).getTimeInMillis(new Date()) / 1000d / 60d);
        } catch (DatatypeConfigurationException e) {
            throw new RuntimeException(e);
        }
    }

    public static int getNumThreads() {
        return 1 + ForkJoinPool.getCommonPoolParallelism(); // the java threads stream on common fork pool PLUS the main thread, hence the addition of 1
        //return ForkJoinPool.getCommonPoolParallelism(); // the java threads stream on common fork without the plus 1
    }

    public static float interpolate(double leftMz, float leftIntensity, double rightMz, float rightIntensity,
                                    double middleMz) {
        if (SAFE_MODE) {
            Preconditions.checkState(rightMz > leftMz,
                    "Your  rightMz of %s was lower or equal to leftMz of %s", leftMz, rightMz);
        }

        if (middleMz > rightMz) middleMz = rightMz;
        else if (middleMz < leftMz) middleMz = leftMz;

        double deltaMz = rightMz - leftMz;
        float deltaIntensity = rightIntensity - leftIntensity;
        double fractionalPartOfMz = (middleMz - leftMz) / deltaMz;

        return (float) (leftIntensity + (fractionalPartOfMz * deltaIntensity));
    }

    public static double[] convertFloatsToDoubles(float[] input) {
        if (input == null) {
            return null; // Or throw an exception - your choice
        }
        double[] output = new double[input.length];
        for (int i = 0; i < input.length; i++) {
            output[i] = input[i];
        }
        return output;
    }

    public static float[] convertDoublesToFloats(double[] input) {
        if (input == null) {
            return null; // Or throw an exception - your choice
        }
        float[] output = new float[input.length];
        for (int i = 0; i < input.length; i++) {
            output[i] = (float) input[i];
        }
        return output;
    }

    public static File getDirectoryOfExecutableJar() throws UnsupportedEncodingException {
        File directoryOfExecutableJar = null;
        final URL applicationRootPathURL = ImportMsFile.class.getProtectionDomain().getCodeSource().getLocation();
        final File applicationRootPath = new File(URLDecoder.decode(applicationRootPathURL.getPath(), "UTF-8"));
        if (applicationRootPath.isDirectory()) {
            directoryOfExecutableJar = applicationRootPath;
        } else {
            directoryOfExecutableJar = applicationRootPath.getParentFile();
        }
        return directoryOfExecutableJar;
    }

    /* fast floating point exp function
 * must initialize table with buildexptable before using

Based on
 A Fast, Compact Approximation of the Exponential Function
 Nicol N. Schraudolph 1999

Adapted to single precision to improve speed and added adjustment table to improve accuracy.
Alrecenk 2014

 * i = ay + b
 * a = 2^(mantissa bits) / ln(2)   ~ 12102203
 * b = (exponent bias) * 2^ ( mantissa bits) ~ 1065353216
 */
//    private static AtomicInteger countGood = new AtomicInteger(0);
//    private static AtomicInteger countBad = new AtomicInteger(0);
    public static float approxExp(float x) {
        //    return (float) FastMath.exp(x);
        if (x < minExp) {
            return minExpValue;
        } else if (x > maxExp) {
            return maxExpValue;
        }
        final int temp = (int) (12102203 * x + 1065353216);
        final float approxExp = Float.intBitsToFloat(temp) * expadjust[(temp >> 15) & 0xff];
        return approxExp;
    }

    //build correction table to improve result in region of interest
//if region of interest is large enough then improves result everywhere
    public static void buildexptable(double min, double max, double step) {
        minExp = min;
        maxExp = max;
        minExpValue = (float) FastMath.exp(min);
        maxExpValue = (float) FastMath.exp(max);
        expadjust = new float[256];
        int amount[] = new int[256];
        //calculate what adjustments should have been for values in region
        for (double x = min; x < max; x += step) {
            double exp = Math.exp(x);
            int temp = (int) (12102203 * x + 1065353216);
            int index = (temp >> 15) & 0xff;
            double fexp = Float.intBitsToFloat(temp);
            expadjust[index] += exp / fexp;
            amount[index]++;
        }
        //average them out to getDGivenIdx adjustment table
        for (int k = 0; k < amount.length; k++) {
            expadjust[k] /= amount[k];
        }
    }

    //TODO make a "resolutionEstimator" class, simliar to emgCoefficients
    public static double estimateFwhm(double mz, float intensity) {
        //FOR now, we've hardcoded this for sciex intstruments
//        final double resolution = 15000;
//        final double fwhm = mz / resolution;
//        return fwhm;

        //TODO remove hardcoding for thermo
        final float ppmTol = 50f;
        return (ppmTol * mz) / (1e6 * Constants.MAX_FRACTION_OF_FWHM_FOR_FEATURE_LINKING);
    }

    public static float approxPow(final float x, final float exp) {
        if (exp == 1f) return x;
        if (exp == 2f) return x * x;
        //  LOG.debug("Can't do fast approx pow since exp is {}; just doing normal FastMath.pow()", exp);
        return (float) FastMath.pow(x, exp); //TODO replace with approximate version
    }

    public static void tryLockFor60Seconds(Lock lock) {
        try {
            lock.tryLock(60, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            throw new RuntimeException("Lock timeout exceeded 60 seconds for lock " + lock);
        }
    }

    public static int getLowerOrEqualPoint(final int idx) {
        return (idx < 0) ? -(idx + 1) - 1 : idx;
    }

    public static String prettyOutputOf2DDoubleArray(double[][] costMatrixAs2DArray) {
        final StringBuilder sb = new StringBuilder();
        sb.append("\nFrom 2d double array: NumNodes: ");
        sb.append(costMatrixAs2DArray.length);
        sb.append("\nw,j,weight\n");
        for (int w = 0, size = costMatrixAs2DArray.length; w < size; w++) {
            for (int j = 0; j < size; j++) {
                sb.append(w);
                sb.append(",");
                sb.append(j);
                sb.append(",");
                sb.append(String.format("%.6f", costMatrixAs2DArray[w][j]));
                sb.append("\n");
            }
        }

        return sb.toString();
    }

    public static String prettyOutputOf1DDoubleArray(double[] costMatrixAs2DArray) {
        final StringBuilder sb = new StringBuilder();
        for (int w = 0, size = costMatrixAs2DArray.length; w < size; w++) {
            sb.append(String.format("%.6f", costMatrixAs2DArray[w]));
            sb.append(",");
        }

        return sb.toString();
    }

    public static String prettyOutputOf1DFloatArray(float[] costMatrixAs2DArray) {
        final StringBuilder sb = new StringBuilder();
        for (int w = 0, size = costMatrixAs2DArray.length; w < size; w++) {
            sb.append(String.format("%.6f", costMatrixAs2DArray[w]));
            sb.append(",");
        }

        return sb.toString();
    }

    public static void sleepInSec(final int secondsToSleep) {
        log.warn("About to sleep for {} seconds", secondsToSleep);
        try {
            Thread.sleep(secondsToSleep * 1000);
        } catch (InterruptedException e) {
            log.warn("Sleeping interupted");
            e.printStackTrace();
        }
    }

    public float roundFloatIntensity(float intensity) {
        return (float) Math.round(intensity * 10f) / 10f;
    }

    public float roundFloatRt(float rt) {
        return (float) Math.round(rt * 100f) / 100f;
    }

    public double roundDoubleMz(double mz) {
        return (double) Math.round(mz * 1000f) / 1000f;
    }

    public double ppmToMass(double ppm, double mass) {
        return (ppm / 1e6) * mass;
    }
}