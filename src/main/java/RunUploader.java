import com.deepdia.realtimeuploader.ImportMsFile;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.cli.*;
import systems.deepsearch.common.util.ProgressCounter;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@Slf4j
public class RunUploader {

    public static void main(String args[]) throws ParseException {
        //  new ImportMsFile(new File("d:/thermo/02_c01_br01_tr02.raw"));
        //    File folder = new File("C:\\Users\\gsaxena888\\Downloads\\DeepSearchMarch30\\samplefiles\\thermo");
        //File folder = new File("C:\\brainLumos");
        CommandLineParser parser = new DefaultParser();
        CommandLine commandLine = parser.parse(prepareOptions(), args);
        String path = commandLine.getOptionValue("p");
        log.info("Start RunUploader updated v2");
        try (Stream<Path> walk = Files.walk(Paths.get(path))) {
            List<Path> paths = walk.filter(f -> Files.isRegularFile(f) && f.toString().endsWith(".raw")).collect(Collectors.toList());
            int numThermoRawFilesInDirectory = paths.size();
            System.out.println("===========> RunUploader numThermoRawFilesInDirectory=" + numThermoRawFilesInDirectory);
            if (numThermoRawFilesInDirectory == 1) {
                new ImportMsFile(paths.get(0).toFile());
            } else if (numThermoRawFilesInDirectory > 1) {
                final ProgressCounter progressCounter = ProgressCounter.of("Processing all Thermo .raw files in directory " + path, numThermoRawFilesInDirectory, 0.01f, 10, true);
                progressCounter.start();
                paths.forEach(f -> {
                    progressCounter.step();

                    new ImportMsFile(f.toFile());
                });
                progressCounter.finish();
            }
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
        log.info("end RunUploader");
    }

    private static Options prepareOptions() {
        Options options = new Options();
        // Required options
        Option pathOption = Option.builder("p").required()
                .longOpt("path")
                .hasArg()
                .build();
        options.addOption(pathOption);
        return options;
    }
}
